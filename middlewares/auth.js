import jwt from 'jwt-simple';
import moment from 'moment';

const TOKEN_SECRET = "secreto";

export default function isAuth(req,res,next) {
  if(!req.headers.authorization){
    return res.status(403).send({message: "No tienes autorización"})
  }

//  const token = req.headers.authorization.split(" ")[1]  Verificar la nueva estructura porque ya no trae el parametro 0
  const payload = jwt.decode(req.headers.authorization, TOKEN_SECRET)

  if (payload.exp <= moment().unix()){
    return res.status(401).send({message: "El token a expirado"})
  }
  
  next()
}
