//TODO: Si no se envian correos hacer source de la key

import Sendgrid from '@sendgrid/mail'
import sengridkey from './sendgridkey'
Sendgrid.setApiKey(sengridkey.SENDGRID_API_KEY)

function sendEmail (messageTo, messageFrom, messageSubject, message)  {
    let msg  = {
        to: messageTo,
        from: messageFrom,
        subject:messageSubject,
        html: message
    }
    Sendgrid
        .send(msg);
}

export default sendEmail;