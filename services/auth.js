import jwt from 'jwt-simple';
import moment from 'moment';

const TOKEN_SECRET = "secreto";

export default function createToken(user){
    let payload = {
        sub:user,
        iat: moment().unix(),
        exp: moment().add(1, "month").unix()
    }
    return jwt.encode(payload, TOKEN_SECRET);
}