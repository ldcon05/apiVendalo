import soap from 'soap';
// Visanet Function to create Transaction

//Functions
function buildParamsOrderVS(order,addressClient ,body) {

    let cybs = cybs_dfprofiler("visanetgt_titonsoft","live")

    // Build Transaction

    let billTo=`
    <requestMessage xmlns="urn:schemas-cybersource-com:transaction-data-1.111">
    <merchantID>${body.credentials.merchantID}</merchantID>
    <merchantReferenceCode>MRC-123</merchantReferenceCode>
    <billTo>
    <firstName>${order.userId.name}</firstName>
    <lastName>${order.userId.lastName}</lastName>
    <street1>${addressClient.addressB}, ${addressClient.city} </street1>
    <city>${body.location.countryCode}</city>
    <state>${body.location.countryName}</state>
    <postalCode>${addressClient.postalCode}</postalCode>
    <country>${body.location.countryCode}</country>
    <email>${order.userId.email}</email>
    <ipAddress>${body.ip}</ipAddress>
    <customerID>${order.userId.user}</customerID>
    </billTo>`
    
    let products = ''

    order.products.forEach((productCart, index) => {
        products += `
        <item id="${index}">
        <unitPrice>${productCart.product.price}</unitPrice>
        <quantity>${productCart.quantity}</quantity>
        </item> `
    });    
    
    let cardInfo = `
    <purchaseTotals>
    <currency>GTQ</currency>
    <grandTotalAmount>${order.total}</grandTotalAmount>
    </purchaseTotals>
    <card>
    <accountNumber>${body.card.numberCard}</accountNumber>
    <expirationMonth>${body.card.month}</expirationMonth>
    <expirationYear>${body.card.year}</expirationYear>
    <cvNumber>${body.card.cvn}</cvNumber>
    </card>
    <ccAuthService run="true"/>
    <ccCaptureService run="true"/>
    <deviceFingerprintID>${cybs}</deviceFingerprintID>
    </requestMessage>
    `
    return {
        _xml: billTo + products + cardInfo
    };
}

function buildWsSecurityVS(user, passToken) {
    let options = {
        passwordType: "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"
    };

    // wsSecurity = new soap.WSSecurity('visanetgt_campustec', 'U+LAkmmj1gFpVBwn22TgqRRj0YaehBA25VS6DeiVkAWwgsajz6X+fOt7DBFT/bJwJwuZ8Qy6ZIGCeBICRMIAxY8Z6CF21GP49Ii2gO/RIrzlmmnhW0pyAAYmuCfxUhR6cCl6Ev8I52zToMRfJlivRJ3cUTltPR25N2TdL1Tr4hGfp8O3aH6KNEkAQOQoCT4hHiPs59uco8SpigY+Wy9ah65vwPpc6ys8YoTpDigsjOKJJ+zyfg2WSbH0YpxDCohot3g6AAGzFnDILxwX0KrPsRKHXpHRqOQw7bvFYE1MMNy+7QjRwNLcottiJhajH8FfX/t0+mtitFRueiA93Vf5xA==', options)
    return new soap.WSSecurity(user, passToken, options);
}

function buildUrlVS (environment) {
    return `https://ics2ws${environment}.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.111.wsdl`;
}

// Cybs
function cybs_dfprofiler(merchantID,environment){
   if (environment == 'live') {
       var org_id = 'k8vif92e';
   } else {
       var org_id = '1snn5n9w';
   }

   return  new Date().getTime();
}

export {buildUrlVS, buildWsSecurityVS, buildParamsOrderVS}