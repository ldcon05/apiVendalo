var cities =  [
    {
        "Codigo": "01",
        "Nombre": "Santa Elena",
        "CodigoDepto": "12",
        "CodigoCabecera": "0027"
    },
    {
        "Codigo": "03",
        "Nombre": "San Benito",
        "CodigoDepto": "12",
        "CodigoCabecera": "0027"
    },
    {
        "Codigo": "11",
        "Nombre": "Melchor de Mencos",
        "CodigoDepto": "12",
        "CodigoCabecera": "0010"
    },
    {
        "Codigo": "07",
        "Nombre": "Santa Ana",
        "CodigoDepto": "12",
        "CodigoCabecera": "0012"
    },
    {
        "Codigo": "05",
        "Nombre": "La Libertad",
        "CodigoDepto": "12",
        "CodigoCabecera": "0017"
    },
    {
        "Codigo": "04",
        "Nombre": "San Andres",
        "CodigoDepto": "12",
        "CodigoCabecera": "0028"
    },
    {
        "Codigo": "12",
        "Nombre": "Poptun",
        "CodigoDepto": "12",
        "CodigoCabecera": "0051"
    },
    {
        "Codigo": "08",
        "Nombre": "Dolores",
        "CodigoDepto": "12",
        "CodigoCabecera": "0032"
    },
    {
        "Codigo": "09",
        "Nombre": "San Luis",
        "CodigoDepto": "12",
        "CodigoCabecera": "0043"
    },
    {
        "Codigo": "01",
        "Nombre": "Retalhuleu",
        "CodigoDepto": "15",
        "CodigoCabecera": "0065"
    },
    {
        "Codigo": "08",
        "Nombre": "Nuevo San Carlos",
        "CodigoDepto": "15",
        "CodigoCabecera": "0064"
    },
    {
        "Codigo": "19",
        "Nombre": "El Palmar",
        "CodigoDepto": "13",
        "CodigoCabecera": "0060"
    },
    {
        "Codigo": "09",
        "Nombre": "El Asintal",
        "CodigoDepto": "15",
        "CodigoCabecera": "0061"
    },
    {
        "Codigo": "05",
        "Nombre": "San Felipe",
        "CodigoDepto": "15",
        "CodigoCabecera": "0068"
    },
    {
        "Codigo": "06",
        "Nombre": "San Andres Villa Seca",
        "CodigoDepto": "15",
        "CodigoCabecera": "0067"
    },
    {
        "Codigo": "04",
        "Nombre": "San Martin Zapotitlan",
        "CodigoDepto": "15",
        "CodigoCabecera": "0069"
    },
    {
        "Codigo": "02",
        "Nombre": "San Sebastian",
        "CodigoDepto": "15",
        "CodigoCabecera": "0070"
    },
    {
        "Codigo": "03",
        "Nombre": "Santa Cruz Mulua",
        "CodigoDepto": "15",
        "CodigoCabecera": "0071"
    },
    {
        "Codigo": "19",
        "Nombre": "Pueblo Nuevo",
        "CodigoDepto": "20",
        "CodigoCabecera": "0073"
    },
    {
        "Codigo": "07",
        "Nombre": "Champerico",
        "CodigoDepto": "15",
        "CodigoCabecera": "0075"
    },
    {
        "Codigo": "01",
        "Nombre": "Mazatenango",
        "CodigoDepto": "20",
        "CodigoCabecera": "0097"
    },
    {
        "Codigo": "09",
        "Nombre": "San Pablo Jocopilas",
        "CodigoDepto": "20",
        "CodigoCabecera": "0082"
    },
    {
        "Codigo": "08",
        "Nombre": "Samayac",
        "CodigoDepto": "20",
        "CodigoCabecera": "0080"
    },
    {
        "Codigo": "05",
        "Nombre": "San Jose El Idolo",
        "CodigoDepto": "20",
        "CodigoCabecera": "0081"
    },
    {
        "Codigo": "17",
        "Nombre": "Santo Tomas La Union",
        "CodigoDepto": "20",
        "CodigoCabecera": "0083"
    },
    {
        "Codigo": "02",
        "Nombre": "Cuyotenango",
        "CodigoDepto": "20",
        "CodigoCabecera": "0085"
    },
    {
        "Codigo": "03",
        "Nombre": "San Francisco Zapotitlan",
        "CodigoDepto": "20",
        "CodigoCabecera": "0090"
    },
    {
        "Codigo": "12",
        "Nombre": "San Gabriel",
        "CodigoDepto": "20",
        "CodigoCabecera": "0088"
    },
    {
        "Codigo": "07",
        "Nombre": "San Lorenzo",
        "CodigoDepto": "20",
        "CodigoCabecera": "0089"
    },
    {
        "Codigo": "18",
        "Nombre": "Zunilito",
        "CodigoDepto": "20",
        "CodigoCabecera": "0090"
    },
    {
        "Codigo": "06",
        "Nombre": "Santo Domingo",
        "CodigoDepto": "20",
        "CodigoCabecera": "0092"
    },
    {
        "Codigo": "13",
        "Nombre": "Chicacao",
        "CodigoDepto": "20",
        "CodigoCabecera": "0096"
    },
    {
        "Codigo": "10",
        "Nombre": "San Antonio Suchitepequez",
        "CodigoDepto": "20",
        "CodigoCabecera": "0099"
    },
    {
        "Codigo": "04",
        "Nombre": "San Bernardino",
        "CodigoDepto": "20",
        "CodigoCabecera": "0100"
    },
    {
        "Codigo": "11",
        "Nombre": "San Miguel Panan",
        "CodigoDepto": "20",
        "CodigoCabecera": "0101"
    },
    {
        "Codigo": "20",
        "Nombre": "Coatepeque",
        "CodigoDepto": "13",
        "CodigoCabecera": "0113"
    },
    {
        "Codigo": "22",
        "Nombre": "Flores Costa Cuca",
        "CodigoDepto": "13",
        "CodigoCabecera": "0104"
    },
    {
        "Codigo": "21",
        "Nombre": "Genova",
        "CodigoDepto": "13",
        "CodigoCabecera": "0105"
    },
    {
        "Codigo": "12",
        "Nombre": "Nuevo Progreso",
        "CodigoDepto": "17",
        "CodigoCabecera": "0107"
    },
    {
        "Codigo": "20",
        "Nombre": "El Quetzal",
        "CodigoDepto": "17",
        "CodigoCabecera": "0108"
    },
    {
        "Codigo": "21",
        "Nombre": "La Reforma",
        "CodigoDepto": "17",
        "CodigoCabecera": "0109"
    },
    {
        "Codigo": "13",
        "Nombre": "El Tumbador",
        "CodigoDepto": "17",
        "CodigoCabecera": "0150"
    },
    {
        "Codigo": "17",
        "Nombre": "Colomba",
        "CodigoDepto": "13",
        "CodigoCabecera": "0113"
    },
    {
        "Codigo": "15",
        "Nombre": "Malacatan",
        "CodigoDepto": "17",
        "CodigoCabecera": "0123"
    },
    {
        "Codigo": "14",
        "Nombre": "El Rodeo",
        "CodigoDepto": "17",
        "CodigoCabecera": "0118"
    },
    {
        "Codigo": "19",
        "Nombre": "San Pablo",
        "CodigoDepto": "17",
        "CodigoCabecera": "0125"
    },
    {
        "Codigo": "11",
        "Nombre": "San Rafael Pie de la Cuesta",
        "CodigoDepto": "17",
        "CodigoCabecera": "0129"
    },
    {
        "Codigo": "17",
        "Nombre": "Ayutla",
        "CodigoDepto": "17",
        "CodigoCabecera": "0154"
    },
    {
        "Codigo": "18",
        "Nombre": "Ocos",
        "CodigoDepto": "17",
        "CodigoCabecera": "0138"
    },
    {
        "Codigo": "16",
        "Nombre": "Catarina",
        "CodigoDepto": "17",
        "CodigoCabecera": "0148"
    },
    {
        "Codigo": "01",
        "Nombre": "San Marcos",
        "CodigoDepto": "17",
        "CodigoCabecera": "0343"
    },
    {
        "Codigo": "22",
        "Nombre": "Pajapita",
        "CodigoDepto": "17",
        "CodigoCabecera": "0152"
    },
    {
        "Codigo": "06",
        "Nombre": "Tiquisate",
        "CodigoDepto": "06",
        "CodigoCabecera": "0171"
    },
    {
        "Codigo": "16",
        "Nombre": "San Juan Bautista",
        "CodigoDepto": "20",
        "CodigoCabecera": "0160"
    },
    {
        "Codigo": "13",
        "Nombre": "Nueva Concepcion",
        "CodigoDepto": "06",
        "CodigoCabecera": "1327"
    },
    {
        "Codigo": "14",
        "Nombre": "Patulul",
        "CodigoDepto": "20",
        "CodigoCabecera": "0174"
    },
    {
        "Codigo": "08",
        "Nombre": "Pochuta",
        "CodigoDepto": "03",
        "CodigoCabecera": "0430"
    },
    {
        "Codigo": "15",
        "Nombre": "Santa Barbara",
        "CodigoDepto": "20",
        "CodigoCabecera": "0169"
    },
    {
        "Codigo": "20",
        "Nombre": "Rio Bravo",
        "CodigoDepto": "20",
        "CodigoCabecera": "0175"
    },
    {
        "Codigo": "01",
        "Nombre": "Santa Cruz del Quiche",
        "CodigoDepto": "14",
        "CodigoCabecera": "0197"
    },
    {
        "Codigo": "13",
        "Nombre": "Santa Maria Nebaj",
        "CodigoDepto": "14",
        "CodigoCabecera": "2992"
    },
    {
        "Codigo": "16",
        "Nombre": "Sacapulas",
        "CodigoDepto": "14",
        "CodigoCabecera": "0179"
    },
    {
        "Codigo": "09",
        "Nombre": "San Pedro Jocopilas",
        "CodigoDepto": "14",
        "CodigoCabecera": "0180"
    },
    {
        "Codigo": "11",
        "Nombre": "San Juan Cotzal",
        "CodigoDepto": "14",
        "CodigoCabecera": "0181"
    },
    {
        "Codigo": "05",
        "Nombre": "Chajul",
        "CodigoDepto": "14",
        "CodigoCabecera": "0182"
    },
    {
        "Codigo": "15",
        "Nombre": "Uspantan",
        "CodigoDepto": "14",
        "CodigoCabecera": "0184"
    },
    {
        "Codigo": "19",
        "Nombre": "Chicaman",
        "CodigoDepto": "14",
        "CodigoCabecera": "0184"
    },
    {
        "Codigo": "10",
        "Nombre": "Cunen",
        "CodigoDepto": "14",
        "CodigoCabecera": "0186"
    },
    {
        "Codigo": "18",
        "Nombre": "Canilla",
        "CodigoDepto": "14",
        "CodigoCabecera": "0187"
    },
    {
        "Codigo": "14",
        "Nombre": "San Andres Sajcabaja",
        "CodigoDepto": "14",
        "CodigoCabecera": "0188"
    },
    {
        "Codigo": "07",
        "Nombre": "Patzite",
        "CodigoDepto": "14",
        "CodigoCabecera": "0189"
    },
    {
        "Codigo": "08",
        "Nombre": "San Antonio Ilotenango",
        "CodigoDepto": "14",
        "CodigoCabecera": "0190"
    },
    {
        "Codigo": "17",
        "Nombre": "San Bartolome Jocotenango",
        "CodigoDepto": "14",
        "CodigoCabecera": "0191"
    },
    {
        "Codigo": "12",
        "Nombre": "Joyabaj",
        "CodigoDepto": "14",
        "CodigoCabecera": "0192"
    },
    {
        "Codigo": "04",
        "Nombre": "Zacualpa",
        "CodigoDepto": "14",
        "CodigoCabecera": "0193"
    },
    {
        "Codigo": "06",
        "Nombre": "Santo Tomas Chichicastenango",
        "CodigoDepto": "14",
        "CodigoCabecera": "0625"
    },
    {
        "Codigo": "03",
        "Nombre": "Chinique",
        "CodigoDepto": "14",
        "CodigoCabecera": "0195"
    },
    {
        "Codigo": "02",
        "Nombre": "Chiche",
        "CodigoDepto": "14",
        "CodigoCabecera": "0198"
    },
    {
        "Codigo": "01",
        "Nombre": "Chimaltenango",
        "CodigoDepto": "03",
        "CodigoCabecera": "0212"
    },
    {
        "Codigo": "11",
        "Nombre": "Acatenango",
        "CodigoDepto": "03",
        "CodigoCabecera": "0201"
    },
    {
        "Codigo": "09",
        "Nombre": "Patzicia",
        "CodigoDepto": "03",
        "CodigoCabecera": "0202"
    },
    {
        "Codigo": "07",
        "Nombre": "Patzun",
        "CodigoDepto": "03",
        "CodigoCabecera": "0203"
    },
    {
        "Codigo": "04",
        "Nombre": "San Juan Comalapa",
        "CodigoDepto": "03",
        "CodigoCabecera": "0204"
    },
    {
        "Codigo": "10",
        "Nombre": "Santa Cruz Balanya",
        "CodigoDepto": "03",
        "CodigoCabecera": "0205"
    },
    {
        "Codigo": "15",
        "Nombre": "Zaragoza",
        "CodigoDepto": "03",
        "CodigoCabecera": "0206"
    },
    {
        "Codigo": "12",
        "Nombre": "San Pedro Yepocapa",
        "CodigoDepto": "03",
        "CodigoCabecera": "0207"
    },
    {
        "Codigo": "02",
        "Nombre": "San Jose Poaquil",
        "CodigoDepto": "03",
        "CodigoCabecera": "0209"
    },
    {
        "Codigo": "05",
        "Nombre": "Santa Apolonia",
        "CodigoDepto": "03",
        "CodigoCabecera": "0210"
    },
    {
        "Codigo": "06",
        "Nombre": "Tecpan Guatemala",
        "CodigoDepto": "03",
        "CodigoCabecera": "0211"
    },
    {
        "Codigo": "16",
        "Nombre": "El Tejar",
        "CodigoDepto": "03",
        "CodigoCabecera": "0213"
    },
    {
        "Codigo": "13",
        "Nombre": "San Andres Iztapa",
        "CodigoDepto": "03",
        "CodigoCabecera": "0217"
    },
    {
        "Codigo": "14",
        "Nombre": "Parramos",
        "CodigoDepto": "03",
        "CodigoCabecera": "0216"
    },
    {
        "Codigo": "03",
        "Nombre": "San Martin Jilotepeque",
        "CodigoDepto": "03",
        "CodigoCabecera": "0218"
    },
    {
        "Codigo": "05",
        "Nombre": "Santo Domingo Xenacoj",
        "CodigoDepto": "16",
        "CodigoCabecera": "0219"
    },
    {
        "Codigo": "04",
        "Nombre": "Sumpango",
        "CodigoDepto": "16",
        "CodigoCabecera": "0220"
    },
    {
        "Codigo": "01",
        "Nombre": "Antigua Guatemala",
        "CodigoDepto": "16",
        "CodigoCabecera": "0223"
    },
    {
        "Codigo": "14",
        "Nombre": "San Juan Alotenango",
        "CodigoDepto": "16",
        "CodigoCabecera": "0232"
    },
    {
        "Codigo": "12",
        "Nombre": "Ciudad Vieja",
        "CodigoDepto": "16",
        "CodigoCabecera": "0224"
    },
    {
        "Codigo": "02",
        "Nombre": "Jocotenango",
        "CodigoDepto": "16",
        "CodigoCabecera": "0225"
    },
    {
        "Codigo": "10",
        "Nombre": "Magdalena Milpas Altas",
        "CodigoDepto": "16",
        "CodigoCabecera": "0226"
    },
    {
        "Codigo": "03",
        "Nombre": "Pastores",
        "CodigoDepto": "16",
        "CodigoCabecera": "0227"
    },
    {
        "Codigo": "07",
        "Nombre": "San Bartolome Milpas Altas",
        "CodigoDepto": "16",
        "CodigoCabecera": "0229"
    },
    {
        "Codigo": "08",
        "Nombre": "San Lucas Sacatepequez",
        "CodigoDepto": "16",
        "CodigoCabecera": "0236"
    },
    {
        "Codigo": "09",
        "Nombre": "Santa Lucia Milpas Altas",
        "CodigoDepto": "16",
        "CodigoCabecera": "0242"
    },
    {
        "Codigo": "06",
        "Nombre": "Santiago Sacatepequez",
        "CodigoDepto": "16",
        "CodigoCabecera": "0244"
    },
    {
        "Codigo": "13",
        "Nombre": "San Miguel Dueñas",
        "CodigoDepto": "16",
        "CodigoCabecera": "0246"
    },
    {
        "Codigo": "16",
        "Nombre": "Santa Catarina Barahona",
        "CodigoDepto": "16",
        "CodigoCabecera": "0247"
    },
    {
        "Codigo": "15",
        "Nombre": "San Antonio Aguas Calientes",
        "CodigoDepto": "16",
        "CodigoCabecera": "0248"
    },
    {
        "Codigo": "11",
        "Nombre": "Santa Maria de Jesus",
        "CodigoDepto": "16",
        "CodigoCabecera": "0223"
    },
    {
        "Codigo": "10",
        "Nombre": "Panajachel",
        "CodigoDepto": "19",
        "CodigoCabecera": "0255"
    },
    {
        "Codigo": "01",
        "Nombre": "Solola",
        "CodigoDepto": "19",
        "CodigoCabecera": "0260"
    },
    {
        "Codigo": "09",
        "Nombre": "San Andres Semetabaj",
        "CodigoDepto": "19",
        "CodigoCabecera": "0256"
    },
    {
        "Codigo": "13",
        "Nombre": "San Lucas Toliman",
        "CodigoDepto": "19",
        "CodigoCabecera": "0258"
    },
    {
        "Codigo": "19",
        "Nombre": "Santiago Atitlan",
        "CodigoDepto": "19",
        "CodigoCabecera": "0259"
    },
    {
        "Codigo": "05",
        "Nombre": "Nahuala",
        "CodigoDepto": "19",
        "CodigoCabecera": "0264"
    },
    {
        "Codigo": "02",
        "Nombre": "San Jose Chacaya",
        "CodigoDepto": "19",
        "CodigoCabecera": "0265"
    },
    {
        "Codigo": "17",
        "Nombre": "San Juan La Laguna",
        "CodigoDepto": "19",
        "CodigoCabecera": "0266"
    },
    {
        "Codigo": "16",
        "Nombre": "San Marcos La Laguna",
        "CodigoDepto": "19",
        "CodigoCabecera": "0267"
    },
    {
        "Codigo": "15",
        "Nombre": "San Pablo La Laguna",
        "CodigoDepto": "19",
        "CodigoCabecera": "0268"
    },
    {
        "Codigo": "18",
        "Nombre": "San Pedro La Laguna",
        "CodigoDepto": "19",
        "CodigoCabecera": "0269"
    },
    {
        "Codigo": "06",
        "Nombre": "Santa Catarina Ixtahuacan",
        "CodigoDepto": "19",
        "CodigoCabecera": "0270"
    },
    {
        "Codigo": "07",
        "Nombre": "Santa Clara La Laguna",
        "CodigoDepto": "19",
        "CodigoCabecera": "0271"
    },
    {
        "Codigo": "14",
        "Nombre": "Santa Cruz La Laguna",
        "CodigoDepto": "19",
        "CodigoCabecera": "0272"
    },
    {
        "Codigo": "04",
        "Nombre": "Santa Lucia Utatlan",
        "CodigoDepto": "19",
        "CodigoCabecera": "0273"
    },
    {
        "Codigo": "03",
        "Nombre": "Santa Maria Visitacion",
        "CodigoDepto": "19",
        "CodigoCabecera": "0274"
    },
    {
        "Codigo": "11",
        "Nombre": "Santa Catarina Palopo",
        "CodigoDepto": "19",
        "CodigoCabecera": "0276"
    },
    {
        "Codigo": "12",
        "Nombre": "San Antonio Palopo",
        "CodigoDepto": "19",
        "CodigoCabecera": "0277"
    },
    {
        "Codigo": "08",
        "Nombre": "Concepcion",
        "CodigoDepto": "19",
        "CodigoCabecera": "0261"
    },
    {
        "Codigo": "01",
        "Nombre": "Huehuetenango",
        "CodigoDepto": "08",
        "CodigoCabecera": "0317"
    },
    {
        "Codigo": "27",
        "Nombre": "Santa Cruz Barillas",
        "CodigoDepto": "08",
        "CodigoCabecera": "0282"
    },
    {
        "Codigo": "21",
        "Nombre": "San Rafael La Independencia",
        "CodigoDepto": "08",
        "CodigoCabecera": "0289"
    },
    {
        "Codigo": "09",
        "Nombre": "La Libertad",
        "CodigoDepto": "08",
        "CodigoCabecera": "0284"
    },
    {
        "Codigo": "16",
        "Nombre": "San Juan Ixcoy",
        "CodigoDepto": "08",
        "CodigoCabecera": "0285"
    },
    {
        "Codigo": "17",
        "Nombre": "San Mateo Ixtatan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0286"
    },
    {
        "Codigo": "18",
        "Nombre": "San Miguel Acatan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0287"
    },
    {
        "Codigo": "23",
        "Nombre": "San SebastiÃ¡n CoatÃ¡n",
        "CodigoDepto": "08",
        "CodigoCabecera": "0289"
    },
    {
        "Codigo": "28",
        "Nombre": "Santa Eulalia",
        "CodigoDepto": "08",
        "CodigoCabecera": "0290"
    },
    {
        "Codigo": "20",
        "Nombre": "San Pedro Soloma",
        "CodigoDepto": "08",
        "CodigoCabecera": "0291"
    },
    {
        "Codigo": "31",
        "Nombre": "Todos Santos Cuchumatanes",
        "CodigoDepto": "08",
        "CodigoCabecera": "0292"
    },
    {
        "Codigo": "08",
        "Nombre": "La Democracia",
        "CodigoDepto": "08",
        "CodigoCabecera": "0294"
    },
    {
        "Codigo": "04",
        "Nombre": "Colotenango",
        "CodigoDepto": "08",
        "CodigoCabecera": "0296"
    },
    {
        "Codigo": "14",
        "Nombre": "Ixtahuacan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0296"
    },
    {
        "Codigo": "13",
        "Nombre": "San Gaspar Ixchil",
        "CodigoDepto": "08",
        "CodigoCabecera": "0298"
    },
    {
        "Codigo": "15",
        "Nombre": "San Juan Atitan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0300"
    },
    {
        "Codigo": "22",
        "Nombre": "San Rafael Petzal",
        "CodigoDepto": "08",
        "CodigoCabecera": "0301"
    },
    {
        "Codigo": "24",
        "Nombre": "San Sebastian Huehuetenango",
        "CodigoDepto": "08",
        "CodigoCabecera": "0302"
    },
    {
        "Codigo": "26",
        "Nombre": "Santa Barbara",
        "CodigoDepto": "08",
        "CodigoCabecera": "0303"
    },
    {
        "Codigo": "26",
        "Nombre": "Sipacapa",
        "CodigoDepto": "17",
        "CodigoCabecera": "0304"
    },
    {
        "Codigo": "05",
        "Nombre": "Concepcion Huista",
        "CodigoDepto": "08",
        "CodigoCabecera": "0305"
    },
    {
        "Codigo": "11",
        "Nombre": "Nenton",
        "CodigoDepto": "08",
        "CodigoCabecera": "0308"
    },
    {
        "Codigo": "07",
        "Nombre": "Jacaltenango",
        "CodigoDepto": "08",
        "CodigoCabecera": "0307"
    },
    {
        "Codigo": "12",
        "Nombre": "San Antonio Huista",
        "CodigoDepto": "08",
        "CodigoCabecera": "0309"
    },
    {
        "Codigo": "25",
        "Nombre": "Santa Ana Huista",
        "CodigoDepto": "08",
        "CodigoCabecera": "0310"
    },
    {
        "Codigo": "19",
        "Nombre": "San Pedro Necta",
        "CodigoDepto": "08",
        "CodigoCabecera": "0311"
    },
    {
        "Codigo": "29",
        "Nombre": "Santiago Chimaltenango",
        "CodigoDepto": "08",
        "CodigoCabecera": "0312"
    },
    {
        "Codigo": "06",
        "Nombre": "Cuilco",
        "CodigoDepto": "08",
        "CodigoCabecera": "0313"
    },
    {
        "Codigo": "02",
        "Nombre": "Aguacatan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0314"
    },
    {
        "Codigo": "03",
        "Nombre": "Chiantla",
        "CodigoDepto": "08",
        "CodigoCabecera": "0315"
    },
    {
        "Codigo": "10",
        "Nombre": "Malacatancito",
        "CodigoDepto": "08",
        "CodigoCabecera": "0318"
    },
    {
        "Codigo": "32",
        "Nombre": "Tectitan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0319"
    },
    {
        "Codigo": "01",
        "Nombre": "Quetzaltenango",
        "CodigoDepto": "13",
        "CodigoCabecera": "0321"
    },
    {
        "Codigo": "15",
        "Nombre": "Huitan",
        "CodigoDepto": "13",
        "CodigoCabecera": "0324"
    },
    {
        "Codigo": "04",
        "Nombre": "San Andres Xecul",
        "CodigoDepto": "21",
        "CodigoCabecera": "0392"
    },
    {
        "Codigo": "11",
        "Nombre": "Concepcion Chiquirichapa",
        "CodigoDepto": "13",
        "CodigoCabecera": "0326"
    },
    {
        "Codigo": "03",
        "Nombre": "Olintepeque",
        "CodigoDepto": "13",
        "CodigoCabecera": "0327"
    },
    {
        "Codigo": "02",
        "Nombre": "Salcaja",
        "CodigoDepto": "13",
        "CodigoCabecera": "0328"
    },
    {
        "Codigo": "12",
        "Nombre": "San Martin Sacatepequez",
        "CodigoDepto": "13",
        "CodigoCabecera": "0329"
    },
    {
        "Codigo": "01",
        "Nombre": "Totonicapan",
        "CodigoDepto": "21",
        "CodigoCabecera": "0397"
    },
    {
        "Codigo": "23",
        "Nombre": "La Esperanza",
        "CodigoDepto": "13",
        "CodigoCabecera": "0331"
    },
    {
        "Codigo": "09",
        "Nombre": "San Juan Ostuncalco",
        "CodigoDepto": "13",
        "CodigoCabecera": "0332"
    },
    {
        "Codigo": "10",
        "Nombre": "San Mateo",
        "CodigoDepto": "13",
        "CodigoCabecera": "0333"
    },
    {
        "Codigo": "04",
        "Nombre": "San Carlos Sija",
        "CodigoDepto": "13",
        "CodigoCabecera": "0334"
    },
    {
        "Codigo": "18",
        "Nombre": "San Francisco La Union",
        "CodigoDepto": "13",
        "CodigoCabecera": "0335"
    },
    {
        "Codigo": "08",
        "Nombre": "San Miguel Siguila",
        "CodigoDepto": "13",
        "CodigoCabecera": "0337"
    },
    {
        "Codigo": "05",
        "Nombre": "Sibilia",
        "CodigoDepto": "13",
        "CodigoCabecera": "0339"
    },
    {
        "Codigo": "13",
        "Nombre": "Almolonga",
        "CodigoDepto": "13",
        "CodigoCabecera": "0340"
    },
    {
        "Codigo": "14",
        "Nombre": "Cantel",
        "CodigoDepto": "13",
        "CodigoCabecera": "0341"
    },
    {
        "Codigo": "16",
        "Nombre": "Zunil",
        "CodigoDepto": "13",
        "CodigoCabecera": "0342"
    },
    {
        "Codigo": "25",
        "Nombre": "San Cristobal Cucho",
        "CodigoDepto": "17",
        "CodigoCabecera": "0344"
    },
    {
        "Codigo": "23",
        "Nombre": "Ixchiguan",
        "CodigoDepto": "17",
        "CodigoCabecera": "0345"
    },
    {
        "Codigo": "24",
        "Nombre": "San Jose Ojetenam",
        "CodigoDepto": "17",
        "CodigoCabecera": "0369"
    },
    {
        "Codigo": "08",
        "Nombre": "Sibinal",
        "CodigoDepto": "17",
        "CodigoCabecera": "0349"
    },
    {
        "Codigo": "07",
        "Nombre": "Tacana",
        "CodigoDepto": "17",
        "CodigoCabecera": "0350"
    },
    {
        "Codigo": "09",
        "Nombre": "Tajumulco",
        "CodigoDepto": "17",
        "CodigoCabecera": "0351"
    },
    {
        "Codigo": "03",
        "Nombre": "Comitancillo",
        "CodigoDepto": "17",
        "CodigoCabecera": "0352"
    },
    {
        "Codigo": "06",
        "Nombre": "Concepcion Tutuapa",
        "CodigoDepto": "17",
        "CodigoCabecera": "0353"
    },
    {
        "Codigo": "05",
        "Nombre": "San Miguel Ixtahuacan",
        "CodigoDepto": "17",
        "CodigoCabecera": "0354"
    },
    {
        "Codigo": "28",
        "Nombre": "Rio Blanco",
        "CodigoDepto": "17",
        "CodigoCabecera": "0355"
    },
    {
        "Codigo": "29",
        "Nombre": "San Lorenzo",
        "CodigoDepto": "17",
        "CodigoCabecera": "0356"
    },
    {
        "Codigo": "10",
        "Nombre": "Tejutla",
        "CodigoDepto": "17",
        "CodigoCabecera": "0357"
    },
    {
        "Codigo": "24",
        "Nombre": "Palestina de los Altos",
        "CodigoDepto": "13",
        "CodigoCabecera": "0358"
    },
    {
        "Codigo": "02",
        "Nombre": "San Pedro Sacatepequez",
        "CodigoDepto": "17",
        "CodigoCabecera": "0370"
    },
    {
        "Codigo": "27",
        "Nombre": "Esquipulas Palo Gordo",
        "CodigoDepto": "17",
        "CodigoCabecera": "0363"
    },
    {
        "Codigo": "04",
        "Nombre": "San Antonio Sacatepequez",
        "CodigoDepto": "17",
        "CodigoCabecera": "0367"
    },
    {
        "Codigo": "03",
        "Nombre": "San Francisco El Alto",
        "CodigoDepto": "21",
        "CodigoCabecera": "0394"
    },
    {
        "Codigo": "05",
        "Nombre": "Momostenango",
        "CodigoDepto": "21",
        "CodigoCabecera": "0389"
    },
    {
        "Codigo": "08",
        "Nombre": "San Bartolo Aguas Calientes",
        "CodigoDepto": "21",
        "CodigoCabecera": "0378"
    },
    {
        "Codigo": "06",
        "Nombre": "Santa Maria Chiquimula",
        "CodigoDepto": "21",
        "CodigoCabecera": "0383"
    },
    {
        "Codigo": "07",
        "Nombre": "Santa Lucia La Reforma",
        "CodigoDepto": "21",
        "CodigoCabecera": "0625"
    },
    {
        "Codigo": "02",
        "Nombre": "San Cristobal",
        "CodigoDepto": "21",
        "CodigoCabecera": "0389"
    },
    {
        "Codigo": "01",
        "Nombre": "Escuintla",
        "CodigoDepto": "06",
        "CodigoCabecera": "0414"
    },
    {
        "Codigo": "09",
        "Nombre": "Puerto San Jose",
        "CodigoDepto": "06",
        "CodigoCabecera": "0432"
    },
    {
        "Codigo": "05",
        "Nombre": "Masagua",
        "CodigoDepto": "06",
        "CodigoCabecera": "0406"
    },
    {
        "Codigo": "03",
        "Nombre": "La Democracia",
        "CodigoDepto": "06",
        "CodigoCabecera": "0404"
    },
    {
        "Codigo": "02",
        "Nombre": "Santa Lucia Cotzumalguapa",
        "CodigoDepto": "06",
        "CodigoCabecera": "0430"
    },
    {
        "Codigo": "08",
        "Nombre": "Guanagazapa",
        "CodigoDepto": "06",
        "CodigoCabecera": "0411"
    },
    {
        "Codigo": "07",
        "Nombre": "La Gomera",
        "CodigoDepto": "06",
        "CodigoCabecera": "0429"
    },
    {
        "Codigo": "04",
        "Nombre": "Siquinala",
        "CodigoDepto": "06",
        "CodigoCabecera": "0431"
    },
    {
        "Codigo": "10",
        "Nombre": "Iztapa",
        "CodigoDepto": "06",
        "CodigoCabecera": "3546"
    },
    {
        "Codigo": "09",
        "Nombre": "Taxisco",
        "CodigoDepto": "18",
        "CodigoCabecera": "0505"
    },
    {
        "Codigo": "08",
        "Nombre": "Chiquimulilla",
        "CodigoDepto": "18",
        "CodigoCabecera": "0502"
    },
    {
        "Codigo": "14",
        "Nombre": "Conguaco",
        "CodigoDepto": "11",
        "CodigoCabecera": "0972"
    },
    {
        "Codigo": "11",
        "Nombre": "Guazacapan",
        "CodigoDepto": "18",
        "CodigoCabecera": "0503"
    },
    {
        "Codigo": "07",
        "Nombre": "San Juan Tecuaco",
        "CodigoDepto": "18",
        "CodigoCabecera": "0502"
    },
    {
        "Codigo": "15",
        "Nombre": "Pasaco",
        "CodigoDepto": "11",
        "CodigoCabecera": "0499"
    },
    {
        "Codigo": "01",
        "Nombre": "Puerto Barrios",
        "CodigoDepto": "09",
        "CodigoCabecera": "0529"
    },
    {
        "Codigo": "03",
        "Nombre": "El Estor",
        "CodigoDepto": "09",
        "CodigoCabecera": "0558"
    },
    {
        "Codigo": "04",
        "Nombre": "Morales",
        "CodigoDepto": "09",
        "CodigoCabecera": "0574"
    },
    {
        "Codigo": "05",
        "Nombre": "Los Amates",
        "CodigoDepto": "09",
        "CodigoCabecera": "0632"
    },
    {
        "Codigo": "02",
        "Nombre": "Livingston",
        "CodigoDepto": "09",
        "CodigoCabecera": "0533"
    },
    {
        "Codigo": "01",
        "Nombre": "Guatemala",
        "CodigoDepto": "07",
        "CodigoCabecera": "1393"
    },
    {
        "Codigo": "11",
        "Nombre": "Palin",
        "CodigoDepto": "06",
        "CodigoCabecera": "0646"
    },
    {
        "Codigo": "12",
        "Nombre": "San Vicente Pacaya",
        "CodigoDepto": "06",
        "CodigoCabecera": "0646"
    },
    {
        "Codigo": "14",
        "Nombre": "Amatitlan",
        "CodigoDepto": "07",
        "CodigoCabecera": "0648"
    },
    {
        "Codigo": "01",
        "Nombre": "Guastatoya",
        "CodigoDepto": "05",
        "CodigoCabecera": "0650"
    },
    {
        "Codigo": "08",
        "Nombre": "San Antonio La Paz",
        "CodigoDepto": "05",
        "CodigoCabecera": "0651"
    },
    {
        "Codigo": "07",
        "Nombre": "Sanarate",
        "CodigoDepto": "05",
        "CodigoCabecera": "0652"
    },
    {
        "Codigo": "06",
        "Nombre": "Sansare",
        "CodigoDepto": "05",
        "CodigoCabecera": "1295"
    },
    {
        "Codigo": "07",
        "Nombre": "San Pedro Ayampuc",
        "CodigoDepto": "07",
        "CodigoCabecera": "0662"
    },
    {
        "Codigo": "05",
        "Nombre": "Palencia",
        "CodigoDepto": "07",
        "CodigoCabecera": "0660"
    },
    {
        "Codigo": "04",
        "Nombre": "San Jose Del Golfo",
        "CodigoDepto": "07",
        "CodigoCabecera": "0661"
    },
    {
        "Codigo": "13",
        "Nombre": "Fraijanes",
        "CodigoDepto": "07",
        "CodigoCabecera": "0667"
    },
    {
        "Codigo": "03",
        "Nombre": "San Jose Pinula",
        "CodigoDepto": "07",
        "CodigoCabecera": "0668"
    },
    {
        "Codigo": "02",
        "Nombre": "Santa Catarina Pinula",
        "CodigoDepto": "07",
        "CodigoCabecera": "0672"
    },
    {
        "Codigo": "16",
        "Nombre": "Villa Canales",
        "CodigoDepto": "07",
        "CodigoCabecera": "0695"
    },
    {
        "Codigo": "06",
        "Nombre": "Santa Cruz El Chol",
        "CodigoDepto": "02",
        "CodigoCabecera": "0674"
    },
    {
        "Codigo": "05",
        "Nombre": "Granados",
        "CodigoDepto": "02",
        "CodigoCabecera": "0675"
    },
    {
        "Codigo": "21",
        "Nombre": "Pachalum",
        "CodigoDepto": "14",
        "CodigoCabecera": "0676"
    },
    {
        "Codigo": "12",
        "Nombre": "Chuarrancho",
        "CodigoDepto": "07",
        "CodigoCabecera": "0677"
    },
    {
        "Codigo": "10",
        "Nombre": "San Juan Sacatepequez",
        "CodigoDepto": "07",
        "CodigoCabecera": "0686"
    },
    {
        "Codigo": "08",
        "Nombre": "Mixco",
        "CodigoDepto": "07",
        "CodigoCabecera": "1065"
    },
    {
        "Codigo": "09",
        "Nombre": "San Pedro Sacatepequez",
        "CodigoDepto": "07",
        "CodigoCabecera": "0687"
    },
    {
        "Codigo": "11",
        "Nombre": "San Raymundo",
        "CodigoDepto": "07",
        "CodigoCabecera": "0688"
    },
    {
        "Codigo": "06",
        "Nombre": "Nueva Chinautla",
        "CodigoDepto": "07",
        "CodigoCabecera": "0689"
    },
    {
        "Codigo": "15",
        "Nombre": "Villa Nueva",
        "CodigoDepto": "07",
        "CodigoCabecera": "0696"
    },
    {
        "Codigo": "17",
        "Nombre": "San Miguel Petapa",
        "CodigoDepto": "07",
        "CodigoCabecera": "0693"
    },
    {
        "Codigo": "05",
        "Nombre": "Teculutan",
        "CodigoDepto": "22",
        "CodigoCabecera": "0801"
    },
    {
        "Codigo": "02",
        "Nombre": "Morazan",
        "CodigoDepto": "05",
        "CodigoCabecera": "0713"
    },
    {
        "Codigo": "05",
        "Nombre": "El Jicaro",
        "CodigoDepto": "05",
        "CodigoCabecera": "0702"
    },
    {
        "Codigo": "03",
        "Nombre": "San Agustin Acasaguastlan",
        "CodigoDepto": "05",
        "CodigoCabecera": "0715"
    },
    {
        "Codigo": "04",
        "Nombre": "San Cristobal Acasaguastlan",
        "CodigoDepto": "05",
        "CodigoCabecera": "0715"
    },
    {
        "Codigo": "07",
        "Nombre": "Cabañas",
        "CodigoDepto": "22",
        "CodigoCabecera": "0743"
    },
    {
        "Codigo": "06",
        "Nombre": "Usumatlan",
        "CodigoDepto": "22",
        "CodigoCabecera": "0801"
    },
    {
        "Codigo": "08",
        "Nombre": "San Diego",
        "CodigoDepto": "22",
        "CodigoCabecera": "2242"
    },
    {
        "Codigo": "10",
        "Nombre": "Huite",
        "CodigoDepto": "22",
        "CodigoCabecera": "0735"
    },
    {
        "Codigo": "02",
        "Nombre": "Morazan",
        "CodigoDepto": "22",
        "CodigoCabecera": "0707"
    },
    {
        "Codigo": "04",
        "Nombre": "Gualan",
        "CodigoDepto": "22",
        "CodigoCabecera": "0762"
    },
    {
        "Codigo": "03",
        "Nombre": "Rio Hondo",
        "CodigoDepto": "22",
        "CodigoCabecera": "0792"
    },
    {
        "Codigo": "01",
        "Nombre": "Zacapa",
        "CodigoDepto": "22",
        "CodigoCabecera": "0839"
    },
    {
        "Codigo": "09",
        "Nombre": "La Union",
        "CodigoDepto": "22",
        "CodigoCabecera": "0771"
    },
    {
        "Codigo": "01",
        "Nombre": "Chiquimula",
        "CodigoDepto": "04",
        "CodigoCabecera": "0843"
    },
    {
        "Codigo": "05",
        "Nombre": "Camotan",
        "CodigoDepto": "04",
        "CodigoCabecera": "0841"
    },
    {
        "Codigo": "04",
        "Nombre": "Jocotan",
        "CodigoDepto": "04",
        "CodigoCabecera": "0848"
    },
    {
        "Codigo": "10",
        "Nombre": "San Jacinto",
        "CodigoDepto": "04",
        "CodigoCabecera": "0858"
    },
    {
        "Codigo": "03",
        "Nombre": "San Juan Ermita",
        "CodigoDepto": "04",
        "CodigoCabecera": "0859"
    },
    {
        "Codigo": "11",
        "Nombre": "Ipala",
        "CodigoDepto": "04",
        "CodigoCabecera": "0870"
    },
    {
        "Codigo": "02",
        "Nombre": "San Jose la Arada",
        "CodigoDepto": "04",
        "CodigoCabecera": "0843"
    },
    {
        "Codigo": "03",
        "Nombre": "San Luis Jilotepeque",
        "CodigoDepto": "10",
        "CodigoCabecera": "0878"
    },
    {
        "Codigo": "07",
        "Nombre": "Esquipulas",
        "CodigoDepto": "04",
        "CodigoCabecera": "0893"
    },
    {
        "Codigo": "08",
        "Nombre": "Concepcion las minas",
        "CodigoDepto": "04",
        "CodigoCabecera": "0888"
    },
    {
        "Codigo": "09",
        "Nombre": "Quezaltepeque",
        "CodigoDepto": "04",
        "CodigoCabecera": "0901"
    },
    {
        "Codigo": "06",
        "Nombre": "Olopa",
        "CodigoDepto": "04",
        "CodigoCabecera": "0906"
    },
    {
        "Codigo": "01",
        "Nombre": "Jutiapa",
        "CodigoDepto": "11",
        "CodigoCabecera": "0937"
    },
    {
        "Codigo": "04",
        "Nombre": "Agua Blanca",
        "CodigoDepto": "11",
        "CodigoCabecera": "0908"
    },
    {
        "Codigo": "03",
        "Nombre": "Santa Catarina Mita",
        "CodigoDepto": "11",
        "CodigoCabecera": "0911"
    },
    {
        "Codigo": "07",
        "Nombre": "Atescatempa",
        "CodigoDepto": "11",
        "CodigoCabecera": "0915"
    },
    {
        "Codigo": "05",
        "Nombre": "Asuncion Mita",
        "CodigoDepto": "11",
        "CodigoCabecera": "0914"
    },
    {
        "Codigo": "09",
        "Nombre": "El Adelanto",
        "CodigoDepto": "11",
        "CodigoCabecera": "0927"
    },
    {
        "Codigo": "06",
        "Nombre": "Yupiltepeque",
        "CodigoDepto": "11",
        "CodigoCabecera": "0931"
    },
    {
        "Codigo": "08",
        "Nombre": "Jerez",
        "CodigoDepto": "11",
        "CodigoCabecera": "0930"
    },
    {
        "Codigo": "10",
        "Nombre": "Zapotitlan",
        "CodigoDepto": "11",
        "CodigoCabecera": "0932"
    },
    {
        "Codigo": "02",
        "Nombre": "El Progreso",
        "CodigoDepto": "11",
        "CodigoCabecera": "0934"
    },
    {
        "Codigo": "17",
        "Nombre": "Quezada",
        "CodigoDepto": "11",
        "CodigoCabecera": "0938"
    },
    {
        "Codigo": "01",
        "Nombre": "Jalapa",
        "CodigoDepto": "10",
        "CodigoCabecera": "0958"
    },
    {
        "Codigo": "06",
        "Nombre": "Monjas",
        "CodigoDepto": "10",
        "CodigoCabecera": "0963"
    },
    {
        "Codigo": "02",
        "Nombre": "San Pedro Pinula",
        "CodigoDepto": "10",
        "CodigoCabecera": "0967"
    },
    {
        "Codigo": "04",
        "Nombre": "San Manuel Chaparron",
        "CodigoDepto": "10",
        "CodigoCabecera": "0957"
    },
    {
        "Codigo": "05",
        "Nombre": "San Carlos Alzatate",
        "CodigoDepto": "10",
        "CodigoCabecera": "0956"
    },
    {
        "Codigo": "02",
        "Nombre": "Barberena",
        "CodigoDepto": "18",
        "CodigoCabecera": "0987"
    },
    {
        "Codigo": "16",
        "Nombre": "San Jose Acatempa",
        "CodigoDepto": "11",
        "CodigoCabecera": "0973"
    },
    {
        "Codigo": "13",
        "Nombre": "Moyuta",
        "CodigoDepto": "11",
        "CodigoCabecera": "0972"
    },
    {
        "Codigo": "12",
        "Nombre": "Jalpatagua",
        "CodigoDepto": "11",
        "CodigoCabecera": "0971"
    },
    {
        "Codigo": "06",
        "Nombre": "Oratorio",
        "CodigoDepto": "18",
        "CodigoCabecera": "0975"
    },
    {
        "Codigo": "13",
        "Nombre": "Pueblo Nuevo Viñas",
        "CodigoDepto": "18",
        "CodigoCabecera": "0977"
    },
    {
        "Codigo": "01",
        "Nombre": "Cuilapa",
        "CodigoDepto": "18",
        "CodigoCabecera": "0996"
    },
    {
        "Codigo": "10",
        "Nombre": "Santa Maria Ixhuatan",
        "CodigoDepto": "18",
        "CodigoCabecera": "0982"
    },
    {
        "Codigo": "03",
        "Nombre": "Santa Rosa de Lima",
        "CodigoDepto": "18",
        "CodigoCabecera": "0983"
    },
    {
        "Codigo": "07",
        "Nombre": "Mataquescuintla",
        "CodigoDepto": "10",
        "CodigoCabecera": "0985"
    },
    {
        "Codigo": "12",
        "Nombre": "Santa Cruz Naranjo",
        "CodigoDepto": "18",
        "CodigoCabecera": "0994"
    },
    {
        "Codigo": "04",
        "Nombre": "Casillas",
        "CodigoDepto": "18",
        "CodigoCabecera": "0989"
    },
    {
        "Codigo": "14",
        "Nombre": "Nueva Santa Rosa",
        "CodigoDepto": "18",
        "CodigoCabecera": "0991"
    },
    {
        "Codigo": "05",
        "Nombre": "San Rafael Las Flores",
        "CodigoDepto": "18",
        "CodigoCabecera": "0993"
    },
    {
        "Codigo": "01",
        "Nombre": "Coban",
        "CodigoDepto": "01",
        "CodigoCabecera": "1005"
    },
    {
        "Codigo": "10",
        "Nombre": "San Juan Chamelco",
        "CodigoDepto": "01",
        "CodigoCabecera": "0998"
    },
    {
        "Codigo": "09",
        "Nombre": "San Pedro Carcha",
        "CodigoDepto": "01",
        "CodigoCabecera": "0999"
    },
    {
        "Codigo": "03",
        "Nombre": "San Cristobal Verapaz",
        "CodigoDepto": "01",
        "CodigoCabecera": "1116"
    },
    {
        "Codigo": "02",
        "Nombre": "Santa Cruz Verapaz",
        "CodigoDepto": "01",
        "CodigoCabecera": "1001"
    },
    {
        "Codigo": "04",
        "Nombre": "Tactic",
        "CodigoDepto": "01",
        "CodigoCabecera": "1002"
    },
    {
        "Codigo": "08",
        "Nombre": "Purulha",
        "CodigoDepto": "02",
        "CodigoCabecera": "1003"
    },
    {
        "Codigo": "07",
        "Nombre": "San Jeronimo",
        "CodigoDepto": "02",
        "CodigoCabecera": "1015"
    },
    {
        "Codigo": "01",
        "Nombre": "Salama",
        "CodigoDepto": "02",
        "CodigoCabecera": "1013"
    },
    {
        "Codigo": "04",
        "Nombre": "Cubulco",
        "CodigoDepto": "02",
        "CodigoCabecera": "1011"
    },
    {
        "Codigo": "03",
        "Nombre": "Rabinal",
        "CodigoDepto": "02",
        "CodigoCabecera": "1012"
    },
    {
        "Codigo": "02",
        "Nombre": "San Miguel Chicaj",
        "CodigoDepto": "02",
        "CodigoCabecera": "1016"
    },
    {
        "Codigo": "15",
        "Nombre": "Fray Bartolome De Las Casas",
        "CodigoDepto": "01",
        "CodigoCabecera": "1024"
    },
    {
        "Codigo": "12",
        "Nombre": "Cahabon",
        "CodigoDepto": "01",
        "CodigoCabecera": "1020"
    },
    {
        "Codigo": "11",
        "Nombre": "Lanquin",
        "CodigoDepto": "01",
        "CodigoCabecera": "1020"
    },
    {
        "Codigo": "13",
        "Nombre": "Chisec",
        "CodigoDepto": "01",
        "CodigoCabecera": "1023"
    },
    {
        "Codigo": "14",
        "Nombre": "Chahal",
        "CodigoDepto": "01",
        "CodigoCabecera": "1022"
    },
    {
        "Codigo": "16",
        "Nombre": "Santa Catalina La Tinta",
        "CodigoDepto": "01",
        "CodigoCabecera": "1026"
    },
    {
        "Codigo": "07",
        "Nombre": "Panzos",
        "CodigoDepto": "01",
        "CodigoCabecera": "1027"
    },
    {
        "Codigo": "08",
        "Nombre": "Senahu",
        "CodigoDepto": "01",
        "CodigoCabecera": "1028"
    },
    {
        "Codigo": "05",
        "Nombre": "Tamahu",
        "CodigoDepto": "01",
        "CodigoCabecera": "1029"
    },
    {
        "Codigo": "06",
        "Nombre": "Tucuru",
        "CodigoDepto": "01",
        "CodigoCabecera": "1031"
    },
    {
        "Codigo": "20",
        "Nombre": "Playa Grande Ixcan",
        "CodigoDepto": "14",
        "CodigoCabecera": "1005"
    },
    {
        "Codigo": "02",
        "Nombre": "San Jose",
        "CodigoDepto": "12",
        "CodigoCabecera": "0024"
    },
    {
        "Codigo": "06",
        "Nombre": "San Francisco",
        "CodigoDepto": "12",
        "CodigoCabecera": "0021"
    },
    {
        "Codigo": "10",
        "Nombre": "Sayaxche",
        "CodigoDepto": "12",
        "CodigoCabecera": "0022"
    },
    {
        "Codigo": "22",
        "Nombre": "Nebaj",
        "CodigoDepto": "14",
        "CodigoCabecera": "2992"
    },
    {
        "Codigo": "11",
        "Nombre": "Comapa",
        "CodigoDepto": "11",
        "CodigoCabecera": "0937"
    },
    {
        "Codigo": "06",
        "Nombre": "Cabrican",
        "CodigoDepto": "13",
        "CodigoCabecera": "0321"
    },
    {
        "Codigo": "07",
        "Nombre": "Cajola",
        "CodigoDepto": "13",
        "CodigoCabecera": "0321"
    },
    {
        "Codigo": "23",
        "Nombre": "Chichicastenango",
        "CodigoDepto": "14",
        "CodigoCabecera": "0194"
    },
    {
        "Codigo": "17",
        "Nombre": "Raxruha",
        "CodigoDepto": "01",
        "CodigoCabecera": "1025"
    },
    {
        "Codigo": "18",
        "Nombre": "Santa Maria Cahabon",
        "CodigoDepto": "01",
        "CodigoCabecera": "1020"
    },
    {
        "Codigo": "19",
        "Nombre": "Teleman",
        "CodigoDepto": "01",
        "CodigoCabecera": "1133"
    },
    {
        "Codigo": "33",
        "Nombre": "San Gaspar Ixchil",
        "CodigoDepto": "08",
        "CodigoCabecera": "0298"
    },
    {
        "Codigo": "34",
        "Nombre": "Aguacatan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0314"
    },
    {
        "Codigo": "35",
        "Nombre": "San Idelfonso Ixtaguacan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0299"
    },
    {
        "Codigo": "36",
        "Nombre": "San Sebastian Coatan",
        "CodigoDepto": "08",
        "CodigoCabecera": "0289"
    },
    {
        "Codigo": "37",
        "Nombre": "Union Cantinil",
        "CodigoDepto": "08",
        "CodigoCabecera": "1701"
    },
    {
        "Codigo": "18",
        "Nombre": "Chinautla",
        "CodigoDepto": "07",
        "CodigoCabecera": "0689"
    },
    {
        "Codigo": "13",
        "Nombre": "Las Cruces",
        "CodigoDepto": "12",
        "CodigoCabecera": "0028"
    },
    {
        "Codigo": "30",
        "Nombre": "Tecun Uman",
        "CodigoDepto": "17",
        "CodigoCabecera": "0356"
    },
    {
        "Codigo": "38",
        "Nombre": "Petatán",
        "CodigoDepto": "08",
        "CodigoCabecera": "3782"
    }
]

function findByName(city) {
    return city.Nombre == "0668"
}

export default function findCodeCity(city) {
    city = city.replace(/á/gi,"a")
    city = city.replace(/é/gi,"e")
    city = city.replace(/í/gi,"i")
    city = city.replace(/ó/gi,"o")
    city = city.replace(/ú/gi,"u")

    let ciudad = cities.find((cityApi) => {
        return cityApi.Nombre.toUpperCase() === city.toUpperCase()
    } )
    return ciudad
}