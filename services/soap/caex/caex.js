// soap.createClientAsync('http://wsqa.caexlogistics.com:1880/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl').then((client) => {
//     return client.ObtenerListadoDepartamentosAsync({
//         Autenticacion: {
//             Login: 'WSTITONSOFT',
//             Password: '1008be41c3b9ea4913276a166b01e2ba'
//         }
//     });
// }).then((result) => {l
//     console.log(result.ResultadoObtenerDepartamentos.ListadoDepartamentos.Departamento);
// });

// // Codigo: '07', Nombre: 'Guatemala' 

// function findByDepto(depto) { 
//     return depto.Nombre.toLowerCase() == "santo domingo xenacoj"
// }

// soap.createClientAsync('http://wsqa.caexlogistics.com:1880/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl')
// .then((client) => {
//     return client.ObtenerListadoMunicipiosAsync({
//         Autenticacion: {
//             Login: 'WSTITONSOFT',
//             Password: '1008be41c3b9ea4913276a166b01e2ba'
//         },
//         CodigoDepto:"16"
//     });
// }).then((result) => {
//     res.status(200).send(result.ResultadoObtenerMunicipios.ListadoMunicipios.Municipio.find(findByDepto)) //Me devuelve el primero en coincidir
// });


// soap.createClientAsync('http://wsqa.caexlogistics.com:1880/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl')
// .then((client) => {
//     return client.ObtenerListadoPobladosAsync({
//         Autenticacion: {
//             Login: 'WSTITONSOFT',
//             Password: '1008be41c3b9ea4913276a166b01e2ba'
//         },
//         CodigoDepto: "07"
//     });
// }).then((result) => {
//     console.log(result.ResultadoObtenerPoblados.ListadoPoblados.Poblado);
// });

// soap.createClientAsync('http://wsqa.caexlogistics.com:1880/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl')
// .then((client) => {
//     return client.ObtenerTiposPiezasAsync({
//         Autenticacion: {
//             Login: 'WSTITONSOFT',
//             Password: '1008be41c3b9ea4913276a166b01e2ba'
//         }
//     });
// }).then((result) => {
//     console.log(result.ResultadoObtenerPiezas.ListadoPiezas.Pieza);
// });

// soap.createClientAsync('http://wsqa.caexlogistics.com:1880/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl')
// .then((client) => {
//     return client.ObtenerTarifaEnvioAsync({
//         Autenticacion: {
//             Login: 'WSTITONSOFT',
//             Password: '1008be41c3b9ea4913276a166b01e2ba'
//         },
//         DatosEnvio: {
//             CodigoPobladoDestino: 2659,
//             CodigoPieza: 2,
//             TipoServicio: 1,
//             PesoTotal: 40,
//             CodigoCredito: 'CAEXP15'
//         }
//     });
// }).then((result) => {
//     console.log(result);
// });

function getAutentication() {
    return {
        Login: 'WSTITONSOFT',
        Password: '1008be41c3b9ea4913276a166b01e2ba'
    }
}

function buildJsonGuia(order, addressClient, cityCode) {
    return {
        Autenticacion: getAutentication(),
        ListaRecolecciones: {
            DatosRecoleccion: {
                RecoleccionID: " ",
                RemitenteNombre: order.companyId.name,
                RemitenteDireccion: order.companyId.address,
                RemitenteTelefono: "",
                DestinatarioNombre: `${order.userId.name} ${order.userId.lastName}`,
                DestinatarioDireccion: `${addressClient.addressB}, ${addressClient.city} `,
                DestinatarioTelefono: order.userId.phone,
                DestinatarioContacto: "",
                ReferenciaCliente1 : "",
                ReferenciaCliente2 : "",
                DestinatarioNIT: order.userId.tributaryId,
                CodigoPobladoDestino: cityCode.CodigoCabecera,
                CodigoPobladoOrigen: "",
                TipoServicio: "1",
                MontoCod: 0,
                FormatoImpresion: "1",
                CodigoCredito: 'CAEXP15',
                Observaciones: order.shippingInstructions,
                Piezas: getProductsJson(order.products)
            }
        }
    }
}

function getProductsJson(products) {
    let jsonProducts = []

    products.forEach((product, index) => {
        jsonProducts.push(
            {
                Pieza:{
                    NumeroPieza: (index + 1),
                    TipoPieza: 2,
                    PesoPieza: (product.product.weight * product.quantity),
                    MontoCod: 0
                }
            }      
        )
    });

    return jsonProducts;
}

function buildJsonShipping(weight, codeCity) {
    return {
        Autenticacion: getAutentication(),
        DatosEnvio: {
            CodigoPobladoDestino: codeCity,
            CodigoPieza: 2,
            TipoServicio: 1,
            PesoTotal: weight,
            CodigoCredito: 'CAEXP15'
        }
    }
}

function buildJsonTracking(noGuia) {
    return {
        Autenticacion: getAutentication(),
        NumeroGuia: `${noGuia}`
    }
}

function getUrl(status) {
    return (status) ? 'http://wsqa.caexlogistics.com:1880/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl' :
                      "https://ws.caexlogistics.com/wsCAEXLogisticsSB/wsCAEXLogisticsSB.asmx?wsdl";
}

export {
    buildJsonGuia, 
    getUrl,
    buildJsonShipping,
    buildJsonTracking
}