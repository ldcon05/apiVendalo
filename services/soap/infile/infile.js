import moment from 'moment';
/**
 * TODO: Lo que se cambia de json enviado es :
 *       nitComprador, nombreComercialComprador, direccionComercialComprador,
 *       telefonoComprador, correoComprador, departamentoComprador, municipioComprador
 *       importeBruto, detalleImpuestosIva, importeNetoGravado
 * 
 * productOrShipping = true / productos
 *                   = false / envio
 */
function generateDTE(order, productOrShipping) {
    
    let detail = productOrShipping ? getProducts(order.products)  :  getShippingProducts(order.products) 
      return {
        dte: {
            usuario: order.companyId.payments.infile.user,
            clave: order.companyId.payments.infile.pass,
            validador:false,
            dte: {
                codigoEstablecimiento: order.companyId.payments.infile.codigoEstablecimiento,
                idDispositivo : "001",
                serieAutorizada: order.companyId.payments.infile.serieAutorizada,
                numeroResolucion: 201656870296413,
                fechaResolucion: order.companyId.payments.infile.fechaResolucion,
                tipoDocumento: order.companyId.payments.infile.tipoDocumento,
                serieDocumento: order.companyId.payments.infile.infileSerie,
                nitGface: order.companyId.payments.infile.nitGFace,
                numeroDocumento: order.companyId.payments.infile.noDocumento,
                fechaDocumento: moment.utc().format('YYYY-MM-DD'),
                fechaAnulacion: moment.utc().format('YYYY-MM-DD'),
                estadoDocumento: order.companyId.payments.infile.estadoDocumento,
                codigoMoneda: 'GTQ',
                tipoCambio: 1.00,
                nitComprador: order.userId.tributaryId,
                nombreComercialComprador: `${order.userId.name} ${order.userId.lastName}`,
                direccionComercialComprador: order.userId.address[0].name,
                telefonoComprador: order.userId.phone,
                correoComprador: order.userId.email,
                regimen2989: false,
                departamentoComprador: 'Guatemala',
                municipioComprador: 'Guatemala',
                importeBruto: (order.total - (order.total * 0.12)).toFixed(2),
                detalleImpuestosIva: (order.total * 0.12).toFixed(2),
                importeNetoGravado: order.total,
                importeDescuento:0,
                importeTotalExento:0,
                importeOtrosImpuestos:0,
                montoTotalOperacion: order.total,
                descripcionOtroImpuesto: 'N/A',
                observaciones: 'N/A',
                nitVendedor: order.companyId.payments.infile.infileNitVendedor,
                NombreComercialRazonSocialVendedor: order.companyId.payments.infile.nombreComercialRazonSocial,
                nombreCompletoVendedor: order.companyId.payments.infile.nombreVendedor,
                direccionComercialVendedor: order.companyId.payments.infile.direccionComercial,
                departamentoVendedor: order.companyId.payments.infile.departamento,
                municipioVendedor: order.companyId.payments.infile.municipio,
                regimenISR: 'RET_DEFINITIVA',
                personalizado_01: 'N/A',
                personalizado_02: 'N/A',
                personalizado_03: order.companyId.payments.infile.logo,
                personalizado_05: 'N/A',
                personalizado_06: 'N/A',
                personalizado_07: 'N/A',
                personalizado_08: 'N/A',
                personalizado_09: 'N/A',
                personalizado_10: 'N/A',
                personalizado_11: 'N/A',
                personalizado_12: 'N/A',
                personalizado_13: 'N/A',
                personalizado_14: 'N/A',
                personalizado_15: 'N/A',
                personalizado_16: 'N/A',
                personalizado_17: 'N/A',
                personalizado_18: 'N/A',
                personalizado_19: 'N/A',
                personalizado_20: 'N/A',
                detalleDte: detail
            }
        }
    }
}

function getProducts (products) {
    let productsOrder = []

    products.forEach(function (product, index) {
        let priceProducto = product.product.offer  ?  product.product.offer: product.product.price;
        productsOrder.push(
            {
                cantidad: product.quantity,
                unidadMedida: 'UND',
                codigoProducto: `02510${index}`,
                descripcionProducto: product.product.name,
                precioUnitario: (priceProducto | 0),
                montoBruto: (priceProducto- (priceProducto * 0.12)).toFixed(2) ,
                montoDescuento: 0,
                importeNetoGravado: (priceProducto | 0),
                detalleImpuestosIva: (priceProducto * 0.12).toFixed(2),
                importeExento: 0,
                otrosImpuestos: 0,
                importeOtrosImpuestos: 0,
                importeTotalOperacion: (product.total | 0 ),
                tipoProducto: 'B',
                personalizado_01: 'N/A',
                personalizado_02: 'N/A',
                personalizado_03: product.product.images[0],
                personalizado_04: 'N/A',
                personalizado_05: 'N/A'
            }
        )
    })

    return productsOrder;
}

function getShippingProducts (products) {
    return [{
        cantidad: 1,
        unidadMedida: 'UND',
        codigoProducto: 6524,
        descripcionProducto: "Gastos por envio de una un cuandro de la tienda x",
        precioUnitario: 10,
        montoBruto: 8.93,
        montoDescuento: 0,
        importeNetoGravado: 10,
        detalleImpuestosIva: 1.07,
        importeExento: 0,
        otrosImpuestos: 0,
        importeOtrosImpuestos: 0,  
        importeTotalOperacion: 10,
        tipoProducto: 'B',
        personalizado_01: 'N/A',
        personalizado_02: 'N/A',
        personalizado_03: 'vendalo.png',
        personalizado_04: 'N/A',
        personalizado_05: 'N/A'
    }]
}

export default generateDTE;