import soap from 'soap';
import { resolve } from 'path';

//Visanet
function createVisanetClient(url, wsSecurity, params, res, company) {
    console.log(url)
    soap.createClient(url, function(err, client) {
        client.setSecurity(wsSecurity)
        client.runTransaction(params, function(err, result) {
            if(result.reasonCode == 100){
                res.status(200).send({message:'Transaccion procesada correctamente'})
           }else {
               res.status(404).send({message:'Transaccion no procesada'})  
           }
        });
    });
}

//Infile
function generateInvoice(params, res) {
    soap.createClient('https://www.ingface.net/listener/ingface?wsdl', function(err, client) {
        client.registrarDte(params, function(err, result) {
            if(result.return.numeroDte)
                res.status(200).send({message:`Factura generada correctamente no. ${result.return.numeroDte}`})
            else
                res.status(404).send(err)
        });
    });
}

//Caex Cotizar Envio 
function getShipping(params, url){
   return new Promise((resolve, reject) => {
        soap.createClient(url, function(err, client) {
            client.ObtenerTarifaEnvio(params, 
            function (err, result) {
                resolve(result)
            })
        })
   })
}


//Caex generar guia
function setGuiaCaex(params, url){
    return new Promise((resolve, reject) => {
        soap.createClient(url, function(err, client) {
            client.GenerarGuia(params, function (err, result) {
                resolve(result)
             })
        })
    })
}

//Ver el estado del envio
function trackingGuia(params, url) {
    return new Promise((resolve, reject) => {
        soap.createClient(url, function(err, client) {
            client.ObtenerTrackingGuia(params, 
            function (err, result) {
                resolve(result)
            })
        })
    })
}

export {createVisanetClient, generateInvoice, getShipping, setGuiaCaex, trackingGuia}