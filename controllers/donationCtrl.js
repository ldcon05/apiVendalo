import Donation from '../models/donation';

function getDonations(req, res, next) {
    Donation 
        .find()
        .then((donations) => {
            res.status(200).send(donations)
        })
        .catch(next)
}

function addDonation(req, res, next) {
    Donation 
        .create(req.body)
        .then((donationStored) => {
            res.status(200).send(donationStored);
        })  
        .catch(next)
}

export {getDonations, addDonation}