import Country from '../models/country';

function getCountries(req, res, next) {
    Country
        .find()
        .then((countries) => {
            res.status(200).send(countries)
        })
        .catch(next)
}

function addCountry (req, res, next) {
    Country
        .create(req.body)
        .then((companyStored) => {
            res.status(200).send(companyStored);
        })
        .catch(next)
}

export {getCountries, addCountry}