import AddressClient from '../models/addressClient';

function getAddressClient(req, res, next) {
    AddressClient
        .find({client: req.params.clientId})
        .populate('country', "name")
        .then((addresses) => {
            res.status(200).send(addresses)
        })
        .catch(next)
}

function addAddressClient(req, res, next) {
    AddressClient
        .create(req.body)
        .then((addressesStored) => {
            res.status(200).send(addressesStored);
        })
        .catch(next)
}

function updateAddressClient(req, res, next) {
    AddressClient
        .findByIdAndUpdate({_id: req.params.addressId},req.body)
        .then((addressUpdated) => {
            if(!addressUpdated)
                res.status(400).send({message:"No se a podido actualizar la dirreccion"})
            res.status(200).send(addressUpdated)
        })
        .catch(next)
}

function deleteAddressClient(req, res, next) {
    AddressClient
        .findByIdAndRemove({_id: req.params.addressId})
        .then((addressDeleted) => {
            if(!addressDeleted)
                res.status(404).send({message: "No se ha podido eliminar la direccion"})

            res.status(200).send({message: `Se elimino correctamente la direccion: ${addressDeleted.name}`})
        })
        .catch(next)
}

export {getAddressClient, addAddressClient, updateAddressClient, deleteAddressClient}