import Bank from '../models/bank';

function getBanks(req, res, next) {
    Bank
        .find()
        .then((banks) => {
            res.status(200).send(banks)
        })
        .catch(next)
}

function addBank (req, res, next) {
    Bank
        .create(req.body)
        .then((bankStored) => {
            res.status(200).send(bankStored);
        })
        .catch(next)
}

export {getBanks, addBank}