import Company from '../models/company';
import sendEmail from '../services/sendgrid';
import newDomainTemplate from '../templates/mail/newOrder';
import {createBucket, uploadS3} from  '../helpers/aws-s3';

const bucketCompanies = 'vendalocompanies'

function getCompanies (req, res, next) {
    Company
        .find()
        .select('name email domain logo colors')
        .populate('country',"name languaje")
        .then((companies) => {
            sendEmail('daniel@insayd.com', 'info@vendalo.com', 'Creación de Dominio', newDomainTemplate);
            res.status(200).send(companies);
        })
        .catch(next)
} 

function getCompany (req,res,next) {
    Company
        .findById(req.params.companyId)
        .select(`name email domain slogan about address
                 country phone logo icon tributaryId 
                 levelAccount status links colors structure
                 socialNetworks tokenSNetwork location postalCode payments`)
        .populate('country',"name languaje")
        .then((wantedCompany) => {
            if (!wantedCompany) 
                return res.status(404).send({message:"La empresa solicitada no ha sido registrada"})
            
            res.status(200).send(wantedCompany)
        })
        .catch(next)    
}

function getCompanyDomain (req, res, next) {
    Company
        .findOne({domain: req.params.domainCompany})
        .select(`name email domain slogan about address
                country phone logo icon tributaryId 
                levelAccount status links colors structure
                socialNetworks location payments`)
        .populate('country',"name languaje")
        .then((wantedCompany) => {
            if (!wantedCompany) 
                return res.status(404).send({message:"La empresa solicitada no ha sido registrada"})
            
            res.status(200).send(wantedCompany)
        })
        .catch(next)    
}

function addCompany (req,res,next) {
    Company
        .create(req.body)
        .then((companyStored)=> {
            res.status(200).send(companyStored)
        })
        .catch(next)
}

function updateCompany (req, res, next) {
    Company
        .findByIdAndUpdate({_id: req.params.companyId},req.body)
        .then((updatedCompany) => {
            if(!updatedCompany)
                res.status(404).send({message: "La empresa no ha podido ser actualizada"})
            
            getCompany(req,res,next);
        })
        .catch(next)
}

function updateTokens (req, res, next) {
    const valores = {};
    
    for (let key in req.body.socialNetworks) {
        valores[`socialNetworks.${key}`] = req.body.socialNetworks[key]
    }

    for (let key in req.body.tokenSNetwork) {
        valores[`tokenSNetwork.${key}`] = req.body.tokenSNetwork[key]
    }

    Company
        .findOneAndUpdate(
            { _id: req.params.companyId },
            {
                "$set": valores
            }
        )
        .then((productUpdated) => {
            if(!productUpdated)
                res.status(404).send("El token no pudo ser actualizado")
            
            res.status(200).send(productUpdated)
        })
        .catch(next)
}

function updatePayments (req, res, next) {
    const valores = {};
    
    for (let key in req.body.payments) {
        valores[`payments.${key}`] = req.body.payments[key]
    }

    Company
        .findOneAndUpdate(
            { _id: req.params.companyId },
            {
                "$set": valores
            }
        )
        .then((companyUpdated) => {
            if(!companyUpdated)
                res.status(404).send("La opcion de pago no pudo ser actualizado")
            
            getCompany(req, res, next)
        })
        .catch(next)
}


function deleteCompany (req, res, next) {
    Company
        .findByIdAndRemove({_id: req.params.companyId})
        .then((deletedCompany) => {
            if (!deletedCompany) 
                return res.status(404).send({message:"La compania no existe y no se puedo eliminar"})
            
            res.status(200).send({message: `La compania ${deletedCompany.name} ha sido removida exitosamente`})
        })
        .catch(next)
}

function uploadLogo (req, res, next) {
    if(uploadS3(req.files.logo.path, `${req.params.companyId}-logo`, bucketCompanies)){
        Company
            .findByIdAndUpdate({ _id: req.params.companyId }, { logo : `https://s3.amazonaws.com/${bucketCompanies}/${req.params.companyId}-logo.png`})
            .then((companyUpdated) => {
                res.status(200).send(companyUpdated)
            })
            .catch(next)   
    }else {
        res.status(404).send("La imagen no pudo ser subida")
    }
}

function uploadIcon (req, res, next) {
    if(uploadS3(req.files.icon.path, `${req.params.companyId}-icon`, bucketCompanies)){
        Company
            .findByIdAndUpdate({ _id: req.params.companyId }, { icon : `https://s3.amazonaws.com/${bucketCompanies}/${req.params.companyId}-icon.png`})
            .then((companyUpdated) => {
                res.status(200).send(companyUpdated)
            })
            .catch(next)
    }else {
        res.status(404).send("La imagen no pudo ser subida")
    }
}

export {getCompanies,addCompany, getCompany,
        getCompanyDomain, updateCompany, deleteCompany, 
        updateTokens, updatePayments, uploadLogo, uploadIcon}
