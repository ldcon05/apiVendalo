import Client from '../models/client';
import bcrypt from 'bcrypt';


import {createBucket, uploadS3} from  '../helpers/aws-s3';

// Select Statitcos

const findOneSelect = `email password name lastName user 
    country phone address postalCode avatar tributaryId buyer seller company authorization`;

// Metodos 

function getClients(req, res, next) {
    Client
        .find()
        .then((clients) => {
            res.status(200).send(clients)
        })
        .catch(next)
}

function getClient(req, res, next) {
    Client
        .findById(req.params.clientId)
        .select(findOneSelect)
        .populate('country', 'name countryCode')
        .populate('address.country', 'name')
        .then((wantedClient) => {
            if (!wantedClient)
                return res.status(404).send({ message: "El cliente no existe" })

            res.status(200).send(wantedClient)
        })
        .catch(next)
}

function updateClient(req, res, next) {
    Client
        .findByIdAndUpdate({ _id: req.params.clientId }, req.body)
        .then((updatedClient) => {
            if (!updatedClient)
                res.status(404).send({ message: "El cliente no ha podido ser actualizado" })

            getClient(req, res, next);
        })
        .catch(next)
}


// Login and Register

function addClient(req, res, next) {
    Client
        .create(req.body)
        .then((clientStored) => {
            res.status(200).send(clientStored);
        })
        .catch(next)
}

function getClientByEmail(req, res, next) {
    Client
        .findOne({$or: [
            { email: req.params.email },
            { user: req.params.email  }
        ]})
        .select(findOneSelect)
        .then((wanterUser) => {
            if (!wanterUser)
                return res.status(404).send({ message: "El correo o nombre de usuario no esta vinculado a ningun usuario" })

            res.status(200).send(wanterUser)
        })
        .catch(next)

}


//TODO: Consultar si al momento de login solo me mandan el password anterior y el nuevo asi evitamos la consulta

function Login(req, res, next) {
    Client
        .findOne({$or: [
            { email: req.body.email },
            { user: req.body.user  }
        ]})
        .select(findOneSelect)
        .populate('country', "name languaje")
        .populate('address.country', 'name')
        .then((userLoggedIn) => {
            if (!userLoggedIn)
                return res.status(404).send({ message: "El correo o nombre de usuario no esta vinculado a ningun usuario" })


            bcrypt
                .compare(req.body.password, userLoggedIn.password)
                .then(function (comparedPassword) {
                    if (!comparedPassword)
                        return res.status(404).send({ message: "El password ingresado es incorrecto" })

                    res.status(200).send(userLoggedIn)
                })
                .catch(next)
        })
        .catch(next)
}

//Cambiar Contraseña 

function updatePassword(req, res, next) {
    Client
        .findById(req.params.clientId)
        .select(findOneSelect)
        .then((wantedClient) => {
            bcrypt
                .compare(req.body.currentPassword, wantedClient.password)
                .then(function (comparedPassword) {
                    if (!comparedPassword)
                        return res.status(404).send({ message: "El password ingresado es incorrecto" })

                    bcrypt
                        .hash(req.body.newPassword, 10)
                        .then((hash) => {
                            req.body = {
                                "password" : hash
                            }
                            updateClient(req, res, next)
                        })  
                        .catch(next)
                })
                .catch(next)
        })
        .catch(next)
} 

// Subir Imagenes

function uploadImage(req, res, next) {
    let bucketName = 'vendalousers';
    if(uploadS3(req.files.avatar.path, req.body.name, bucketName)){
        Client
            .findByIdAndUpdate({ _id: req.params.clientId }, { "avatar": `https://s3.amazonaws.com/${bucketName}/${req.body.name}.png` })
            .then((clientUpdated) => {
                res.status(200).send("Client Updated")
            })
            .catch(next)   
    }else {
        res.status(404).send("La imagen no pudo ser subida")
    }
}

export { getClients, getClient, addClient, getClientByEmail, Login, updateClient, uploadImage , updatePassword}