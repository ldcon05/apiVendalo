import Tag from '../models/tag';

function getTags(req, res, next) {
    Tag 
        .find()
        .then((tags) => {
            res.status(200).send(tags)
        })
        .catch(next)
}

function addTag(req, res, next) {
    Tag 
        .create(req.body)
        .then((tagStored) => {
            res.status(200).send(tagStored);
        })  
        .catch(next)
}

export {getTags, addTag}