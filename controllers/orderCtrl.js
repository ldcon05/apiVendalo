import Order from '../models/order';
import AddressClient from '../models/addressClient';
import {buildUrlVS, buildWsSecurityVS, buildParamsOrderVS} from '../services/soap/visanet/visanet';
import generateDTE from '../services/soap/infile/infile'
import findCodeCity from '../services/soap/caex/caexmun'
import {buildJsonGuia, getUrl, buildJsonShipping, buildJsonTracking} from '../services/soap/caex/caex'
import {createVisanetClient, generateInvoice, getShipping, setGuiaCaex, trackingGuia} from '../services/soap/soap';
import soap from 'soap';
import moment from 'moment';
import md5 from 'crypto-md5';

const caexStatus = true;

// Funciones
function getOrders(req, res, next) {
    Order 
        .find()
        .then((orders)=> {
            res.status(200).send(orders)
        })
        .catch(next)
}

function getOrder(req, res, next) {
    Order
        .findById(req.params.orderId)
        .populate('userId','email name user lastName postalCode address')
        .populate('products.product', 'name images price offer')
        .then((order) => {
            res.status(200).send(order)
        })
        .catch(next)
}

function getOrdersByClient(req, res, next) {
    Order 
        .find({userId: req.params.clientId})
        .populate('userId','email name user lastName postalCode address')
        .then((orders) => {
            res.status(200).send(orders)
        })
        .catch(next)
}

function payWithCard(req,res,next) {
    Order
        .findById(req.params.orderId)
        .populate('userId','email name user lastName postalCode')
        .populate('products.product', 'name images price offer')
        .then((orderReturned)=> {
            AddressClient
                .findById(orderReturned.address)
                .populate('country', "name")
                .then((addresses) => {
                    createVisanetClient(
                        buildUrlVS(req.body.credentials.environment),
                        buildWsSecurityVS("visanetgt_titonsoft", "7BZXHimpwF9nLf9MOjXSeyvj6BvYQ5qFHZir+ZR/u68qJ53ItZim26TAV7WBiLN9FznlkmScjGK2mTkbB8RrwC4ANNg8AHt46vxDKAMyiYoYyaGi+RQDPxZWVQE7BaPRUbw6PLTSOOJlpdARVgosXx3RGijzJkWpNHHN/kw+ogjGd/KnkVnIsSFmfVGpRUM7PK53naCuLmvbv0NmA/yK5i6LolOfK29t5o6RtEUGVQ+3T023m9ViKGc9pgMdIO7HB4GJ6bToB0WzELSglNd+8KpNGkbqjBzHwXTlM/K9afDzNp+j9YG/0OefcgCSZhWutQmvuj/fUBaAjRGbs2SVmA=="),
                        buildParamsOrderVS(orderReturned,addresses, req.body),
                        res
                    )
                })
                .catch(next)
        })  
        .catch(next)
}

function buildInvoiceProducts (req, res, next) {
    Order
    .findById(req.params.orderId)
        .populate('userId','email name user lastName phone tributaryId postalCode address')
        .populate('products.product', 'name images price offer')
        .populate('companyId', 'payments')
        .then((orderReturned)=> {
            generateInvoice(generateDTE(orderReturned, true), res)
        })  
        .catch(next)
}

function getCodeCity(req, res, next) {
    res.status(200).send(findCodeCity(req.body.city))
}

function getShippingPackage(req, res, next) {
    Order
        .findById(req.params.orderId)
        .populate('products.product', 'name weight')
        .then((orderReturned)=> {
            let finalShippingPrice = 0;
            orderReturned.products.forEach((productOrder, index) => {
                getShipping(buildJsonShipping(productOrder.product.weight.value , req.body.cityCode), getUrl(caexStatus))
                    .then(shippingPrice => {
                        Order
                            .findOneAndUpdate({_id: req.params.orderId},
                                {
                                    "$inc": {
                                        shipping: (shippingPrice.ResultadoObtenerTarifa.MontoTarifa * productOrder.quantity)
                                    }
                                }
                            )
                            .then((incShipping) => {
                            })
                            .catch(next)
                    })  
            })
        })
        .catch(next)
        res.status(200).send("Shipping añadido")
}

function buildShipping(req, res, next) {
    Order
        .findById(req.params.orderId)
        .populate('userId','email name user lastName phone tributaryId  ')
        .populate('products.product', 'name weight')
        .populate('companyId', 'name email address tributaryId phone')
        .then((orderReturned) => {
            AddressClient
                .findById(orderReturned.address)
                .populate('country', "name")
                .then((addresses) => {
                    setGuiaCaex(buildJsonGuia(orderReturned, addresses, findCodeCity(addresses.city)), getUrl(caexStatus))
                        .then((guia) => {
                            Order
                                .findOneAndUpdate({_id: req.params.orderId},
                                    {
                                        GuiaCaex: {
                                            noGuia: guia.ResultadoGenerarGuia.ListaRecolecciones.DatosRecoleccion[0].NumeroGuia,
                                            sticker: guia.ResultadoGenerarGuia.ListaRecolecciones.DatosRecoleccion[0].URLConsulta
                                        }                            
                                    }
                                )
                                .then((updatedGuia) => {
                                    if(!updatedGuia)
                                        res.status(404).send("La guia no fue generada")
                                    res.status(200).send(`El numero de guia para su orden es: ${guia.ResultadoGenerarGuia.ListaRecolecciones.DatosRecoleccion[0].NumeroGuia} `)
                                })
                                .catch(next)
                        })
                })
                .catch(next)
        })
        .catch(next)
}

function createInvoice (req, res, next) {
}

function getTrackingGuia(req, res, next) {
    trackingGuia(buildJsonTracking(req.params.guia), getUrl(caexStatus))
        .then((tracking) => {
            res.status(200).send(tracking)
        })
}


function addOrder(req, res, next) {
    Order
        .create(req.body)
        .then((orderStored) => {
            res.status(200).send(orderStored);
        })
        .catch(next)
}

function updateOrder (req, res, next) {
    Order
        .findOneAndUpdate({_id: req.params.orderId},req.body)
        .then((orderUpdated) => {
            if(!orderUpdated) 
                res.status(404).send({message: "La orden no ha podido ser actualizada"})
            res.status(200).send(orderUpdated)
        })
        .catch(next)

}

export { getOrders, addOrder, payWithCard, createInvoice, getOrder, 
        getOrdersByClient, updateOrder, buildInvoiceProducts, getCodeCity,
        getShippingPackage, buildShipping, getTrackingGuia}