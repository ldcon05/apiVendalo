import Product from '../models/product';
import {createBucket, uploadS3, removeObjectS3} from  '../helpers/aws-s3';

const selectAllProduct = 'name images price offer coin colors quantity';
const bucketName = 'vendaloproducts'
const pagination = 6;


function getProducts (req,res,next) {
    Product
        .find()
        .select(selectAllProduct)
        .then((products) => {
            res.status(200).send(products)
        })
        .catch(next)
}

function getProduct (req, res, next) {
    Product
        .findById(req.params.productId)
        .then((productStored) => {
            if(!productStored)
                res.status(404).send({message:"El producto solicitado no ha sido encontrado"})
            
            productStored.images.sort()
            res.status(200).send(productStored);
        })
        .catch(next)
}

function getProductByCompany (req, res, next) {
    Product
        .find({company: req.params.companyId})
        .select(selectAllProduct)
        .limit(pagination)
        .skip((req.params.pagination-1) * pagination)   
        .then((productStored) => {
            if(!productStored)
                res.status(404).send({message:"Esta empresa no cuenta con productos"})
            
            res.status(200).send(productStored);
        })
        .catch(next)
}


function getProductByCategory(req, res, next) {
    Product
        .find({category: req.params.categoryId})
        .select(selectAllProduct)
        .limit(pagination)
        .skip((req.params.pagination-1) * pagination)   
        .then((productStored) => {
            if(!productStored)
                res.status(404).send({message:"Esta categoria no cuenta con productos"})
            
            res.status(200).send(productStored);
        })
        .catch(next)
}

function countProducts(req, res, next) {
    Product
        .count({company: req.params.companyId})
        .then((productsCount) => {
            res.status(200).send({"countProducts": productsCount})
        })
        .catch(next)
}

function findProduct(req, res, next) {
    Product
        .find({
            $and: [
                {
                    $or: [
                        {name: { $regex: `${req.params.keyword}` , $options:"si" } },
                        {description: { $regex: `${req.params.keyword}` , $options:"si" } }
                    ]
                },
                {
                    company: req.params.companyId
                }
            ]
            
        })
        .select(selectAllProduct)
        .limit(pagination)
        .skip((req.params.pagination-1) * pagination)   
        .then((productsFound) => {
            if(!productsFound)
                res.status(404).send({message:`No se han encontrado productos en la busqueda de: ${req.params.keyword}` })
            res.status(200).send(productsFound);
        })
        .catch(next)
}

function customSearch(req, res ,next) {
    Product
        .find({
            $and: [
                {
                    $or: [
                        {name: { $regex: `${req.params.keyword}` , $options:"si" } },
                        {description: { $regex: `${req.params.keyword}` , $options:"si" } }
                    ]
                },
                {
                    $or: [
                        {
                            $and: [
                                {price: { $gt: req.params.greater} },
                                {price: { $lte: req.params.less} }
                            ]
                        },
                        {
                            $and: [
                                {offer: { $gt: req.params.greater} },
                                {offer: { $lte: req.params.less} }
                            ]
                        }
                    ]
                },
                {
                    company: req.params.companyId
                }
            ]
        })
        .select(selectAllProduct)
        .limit(pagination)
        .skip((req.params.pagination-1) * pagination)   
        .then((productsFound) => {
            if(!productsFound)
                res.status(404).send({message:`La busqueda no ha generado resultados` })
            res.status(200).send(productsFound);
        })
        .catch(next)
}




function addProduct (req,res,next) {
    Product
        .create(req.body)
        .then((productStored) => {
            
            res.status(200).send(productStored)
        })
        .catch(next)
}


function updateProduct (req, res, next) {
    Product
        .findByIdAndUpdate({_id: req.params.productId},req.body)
        .then((updatedProduct) => {
            if(!updatedProduct)
                res.status(404).send({message: "El producto no pudo ser actualizado"})
            
            getProduct(req,res,next);
        })
        .catch(next)
}


function deleteProduct (req, res, next) {
    Product
        .findByIdAndRemove({_id: req.params.productId})
        .then((deletedProduct) => {
            if (!deletedProduct) 
                return res.status(404).send({message:"El producto no pudo ser  eliminado"})
            
            res.status(200).send({message: `El producto ${deletedProduct.name} ha sido removido exitosamente`})
        })
        .catch(next)
}

function deleteImage (req, res, next) {
    let bandera = true;
    req.body.productimages.forEach((product, index) => {
        if(removeObjectS3(`${product}`, bucketName)){
            Product
                .findByIdAndUpdate(
                    {_id: req.params.productId},
                    {"$pull": 
                        {"images": `https://s3.amazonaws.com/${bucketName}/${product}`}
                    }
                )
                .then((productUpdated)=> {
                })
                .catch(next)
        }else (
            bandera = false
        )
    })
    if(bandera)
        res.status(200).send({message: 'Imagenes eliminadas correctamente'})
    else
        res.status(404).send({message: `La imagenes no pudieron ser eliminadas`})
    
}

function uploadImage (req, res, next) {
    let bandera = true;

    if(req.files.productimages.length > 1) {
        req.files.productimages.forEach((product, index) => {
          bandera =  sendToAWSS3(product, index, req, next)
        })
    }else {
        bandera = sendToAWSS3(req.files.productimages, 0, req, next)
    }
    
    if(bandera)
        res.status(200).send({message: 'Subido correctamente'})
    else
        res.status(404).send({message: `Error al subir`})

} 

function sendToAWSS3 (product, index, req, next) {
    if(uploadS3(product.path, `${req.params.productId}-${index + parseInt(req.body.productnumber)}`, bucketName)){
        Product
            .findByIdAndUpdate(
                {_id: req.params.productId},
                {"$push": 
                    {"images": `https://s3.amazonaws.com/${bucketName}/${req.params.productId}-${index + parseInt(req.body.productnumber)}.png`}
                }
            )
            .then((productUpdated)=> {
            })
            .catch(next)
    }else{
        return false
    } 
    return true
}

function uploapColor (req, res, next) {
    let bandera = true;

    if(req.files.productcolors.length > 1) {
        req.files.productcolors.forEach((color, index) => {
          bandera =  sendColorAWS3(color, index, req, next)
        })
    }else {
        bandera = sendColorAWS3(req.files.productcolors, 0, req, next)
    }
    
    if(bandera)
        res.status(200).send({message: 'Subido correctamente'})
    else
        res.status(404).send({message: `Error al subir`})

}

function sendColorAWS3 (color, index, req, next) {
    if(uploadS3(color.path, `${req.params.productId}-color-${index + parseInt(req.body.productnumber)}`, 'vendaloproductcolors')){
        Product
            .findByIdAndUpdate(
                {_id: req.params.productId},
                {"$push": 
                    {"colors": `https://s3.amazonaws.com/vendaloproductcolors/${req.params.productId}-color-${index + parseInt(req.body.productnumber)}.png`}
                }
            )
            .then((productUpdated)=> {
            })
            .catch(next)
    }else{
        return false
    } 
    return true
}

function deleteCustomColor (req, res, next) {
    let bandera = true;
    req.body.productcolors.forEach((product, index) => {
        if(removeObjectS3(`${product}`, bucketName)){
            Product
                .findByIdAndUpdate(
                    {_id: req.params.productId},
                    {"$pull": 
                        {"colors": product}
                    }
                )
                .then((productUpdated)=> {
                })
                .catch(next)
        }else (
            bandera = false
        )
    })
    if(bandera)
        res.status(200).send({message: 'Colores eliminados correctamente'})
    else
        res.status(404).send({message: `Los colores no pudieron ser eliminados`})
    
}

function uploapColorNoImage (req, res, next) {
    let bandera = true;
    req.body.productcolors.forEach((color, index) => {
        Product
            .findByIdAndUpdate(
                {_id: req.params.productId},
                {"$push": 
                    {"colors": color}
                }
            )
            .then((productUpdated)=> {
                bandera = productUpdated ? true : false
            })
            .catch(next)
    })
    if(bandera)
        res.status(200).send({message: 'Colores agregados correctamente'})
    else
        res.status(404).send({message: `Los colores no pudieron ser agregados`})
}



export {getProducts, getProduct, addProduct, getProductByCompany, 
        updateProduct, deleteProduct, uploadImage, getProductByCategory,
        findProduct, customSearch, deleteImage, uploapColor, deleteCustomColor, 
        uploapColorNoImage, countProducts}