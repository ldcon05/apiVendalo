import Cart from '../models/cart';

const selectAllProduct = 'name images price offer coin';

function getCart(req, res, next) {
    Cart
        .find()
        .populate('products.product', selectAllProduct)
        .then((cartStored)=>{
            res.status(200).send(cartStored)
        })
        .catch(next)
}

function addCart(req, res, next) {
    Cart
        .create(req.body)
        .then((cartCreated)=> {
            res.status(200).send(cartCreated)
        })
        .catch(next)   
}

function getCartWithClientId(req, res, next) {
    Cart
        .findOne({client: req.params.clientId})
        .populate('products.product')
        .then((cartStored) =>{
            res.status(200).send(cartStored)
        })
        .catch(next)
}

function getCartById(req, res, next) {
    Cart
        .findById(req.params.cartId)
        .populate('products.product')
        .then(function (cartStored) {
            res.status(200).send(cartStored)
        })
        .catch(next)
}

function updateCart(req, res, next) {
    Cart
        .findByIdAndUpdate({_id: req.params.cartId},req.body)
        .then((updatedCart) => {
            if(!updatedCart)
                res.status(404).send({message: "El carrito no ha podido ser actualizado"})

            getCartById(req,res,next);
        })
        .catch(next)
}

function calcularTotal (req, res, next) {
    let total = 0;

    Cart
        .findById(req.params.cartId)
        .populate('products.product')
        .then(function (cartStored) {
           for (let index = 0; index < cartStored.products.length; index++) {
               total += cartStored.products[index].total
           }
           req.body = {
               "total" :total
           }
           updateCart(req,res,next);
        })
        .catch(next)
}

function addProductCart (req, res, next) {
    Cart
        .findByIdAndUpdate({_id: req.params.cartId},{"$push": {"products":req.body}})
        .then((updatedCart) => {
            if(!updatedCart)
                res.status(404).send({message:"No se ha actualizado el carrito"})

            calcularTotal(req, res, next);
        })
        .catch(next)
}

function removeProductCart (req, res, next) {
    Cart
        .findByIdAndUpdate({_id: req.params.cartId},{"$pull": {"products": {"product": {"_id":req.params.productId}}}})
        .then((updatedCart) => {
            if(!updatedCart)
                res.status(404).send({message:"No se ha removido el producto del carrito"})

            res.status(200).send({message:"El producto ha sido eliminado correctamente"})
        })
        .catch(next)
}

function removeCart(req, res, next) {
    Cart  
        .findByIdAndRemove({_id: req.params.cartId})
        .then((cartStatus)=> {
            if(!cartStatus)
                res.status(404).send({message: "El carrito no se ha podido remover"})
            
            res.status(200).send({message: "El carrito se removio correctamente"})
        })
        .catch(next)
}


export {getCart, addCart, getCartWithClientId, addProductCart, updateCart, removeProductCart, removeCart}
