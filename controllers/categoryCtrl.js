import Category from '../models/category';

function getCategories(req, res, next) {
    Category 
        .find({company: req.params.companyId})
        .then((categories) => {
            res.status(200).send(categories)
        })
        .catch(next)
}

function findCategoryByName(req, res ,next) {
    Category
        .findOne({
            $and: [
                {company: req.params.companyId},
                {name: req.params.categoryName}
            ]
        })
        .then((categoryFound) => {
            res.status(200).send(categoryFound._id)
        })
        .catch(next)
}

function addCategory(req, res, next) {
    Category 
        .create(req.body)
        .then((categoryStored) => {
            res.status(200).send(categoryStored);
        })  
        .catch(next)
}

function deleteCategory (req, res, next) {
    Category
        .findByIdAndRemove({_id: req.params.categoryId})
        .then((deletedCategory) => {
            if(!deletedCategory)
                res.status(404).send({message: 'La categoria no ha podido ser eliminada'})
            
            res.status(200).send({message: `La categoria ${deletedCategory.name} fue eliminada exitosamente`})
        })
}

export {getCategories, addCategory, findCategoryByName, deleteCategory}