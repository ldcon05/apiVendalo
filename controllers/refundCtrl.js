import Refund from '../models/refund';

function getRefunds(req, res, next) {
    Refund 
        .find()
        .then((refunds) => {
            res.status(200).send(refunds)
        })
        .catch(next)
}

function addRefund(req, res, next) {
    Refund 
        .create(req.body)
        .then((refundStored) => {
            res.status(200).send(refundStored);
        })  
        .catch(next)
}

export {getRefunds, addRefund}