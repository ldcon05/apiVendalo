import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const refundSchema = Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client',
        required:true
    },
    date: {
        type:Date,
        default: Date.now
    },
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },
    order: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Order'
    },
    comment: {
        type:String,
        required:true
    },
    image: {
        type:String,
        required:true
    },
    status:{
        type:Boolean,
        required:true
    }
});

export default mongoose.model('Refund', refundSchema);