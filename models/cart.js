import mongoose from 'mongoose';
import mongooseDouble from 'mongoose-double';
mongooseDouble(mongoose);

const Schema = mongoose.Schema;

const cartScheme = mongoose.Schema({
    client:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Client',
        unique:true
    },
    products:{
        type: [{
            quantity: {
                type:Number,
                required:true
            },
            total: {
                type: mongoose.Schema.Types.Double,
                required:true
            },
            color: {
                type:String
            },
            size: {
                type:String
            },
            product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Product"
            }
        }],
    },
    total: {
        type: mongoose.Schema.Types.Double
    }
});

export default mongoose.model('Cart', cartScheme);