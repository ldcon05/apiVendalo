import mongoose from 'mongoose';
import mongooseDouble from 'mongoose-double';
mongooseDouble(mongoose);

const Schema = mongoose.Schema;

const productSchema = Schema({
    name: {
        type:String,
        required: true
    },
    category: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'Category',
        required: true
    },
    tags: {
        type:[mongoose.Schema.Types.ObjectId],
        ref: 'Tag',
        required:true
    },
    price: {
        type: mongoose.Schema.Types.Double,
        required:true
    },
    offer: {
        type: mongoose.Schema.Types.Double
    },
    description:{
        type:String,
        required: true
    },
    descriptionHome:{
        bedroom: Number,
        bed: Number,
        bathroom:Number,
        kitchen: Boolean,
        tv:Boolean,
        internet:Boolean,
        pool:Boolean,
        parkingLot:Boolean,
        pets:Boolean
    },
    descriptionAutos:{
        gas:String,
        transmission:String,
        origin:String,
        upholstery:String,
        motor:String,
        cylinders:String,
        km: mongoose.Schema.Types.Double,
        door: Number,
        airbag: Boolean,
        stereo: Boolean,
        airConditioned:Boolean
    },
    descriptionDress:{
        cloth: String
    },
    images: { // TODO: Serian imagenes no relacionadas con colores
        type:[String]
    },
    colors:{ // TODO: Seria [{rojo,imagen}, {azul,imagen}]
        type:Array,
    },
    tallas:{
        type:[String] 
    },
    weight: {
        type: mongoose.Schema.Types.Double,
        required:true
    },
    dateOfIssue : {
        type: Date,
        default: Date.now
    },
    quantity: {
        type: Number,
        required:true
    },
    coin: {
        type:String,
        required: true
    },
    taxes: {
        type:Array,
        required: true
    },
    dimensions: {
        width: mongoose.Schema.Types.Double,
        height: mongoose.Schema.Types.Double,
        depth: mongoose.Schema.Types.Double
    },
    score:{
        type:Number
    },
    deliverable: {
        type: Boolean,
        required:true
    },
    views: {
        type: Number,
    },
    company:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company'
    }
});

export default mongoose.model('Product',productSchema);