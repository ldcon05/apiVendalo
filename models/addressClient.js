// TODO: Nuevo esquema , Nombre, direccion, ciudad, provincia o region  , codigo postal y pais

import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const addressSchema = Schema({
    name: {
        type:String,
        required:true
    },
    addressB: {
        type:String,
        required:true
    },
    city: {
        type:String,
        required:true 
    },
    region: {
        type:String,
        required:true
    },
    country:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Country'
    },
    postalCode:String,
    client:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Client'
    },
});

export default mongoose.model('addressClient', addressSchema);