import mongoose from 'mongoose';
import sendEmail from '../services/sendgrid';
import newDomainTemplate from '../templates/mail/newDomain';


const Schema = mongoose.Schema;

const companySchema = Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true,
        unique: true,
        lowercase: true    
    },
    domain:{
        type:String,
        required:true,
        lowercase:true,
        unique:true
    },
    slogan:{
        type:String
    },
    about:{
        type:String
    },
    address:{
        type:String,
        required:true
    },
    country:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Country',
        required:true
    },
    phone: {
        type:Number,
        required:true
    },
    logo:{
        type:String,
    },
    icon:{
        type:String,
    },
    tributaryId: {
        type:String
    },
    typeOfCompany: {
        type:Number
    },
    status: {
        type:Boolean
    },
    links: {
        android:String,
        ios:String
    },
    colors:{
        bg1:String,
        bg2:String,
        color1:String,
        color2:String,  
    },
    structure: {
        header:String,
        content: String,
        aside:String
    },
    socialNetworks: {
        facebook:String,
        twitter:String,
        instagram:String,
        linkedin:String,
        page:String
    },
    tokenSNetwork:{
        fbToken: {},
        twToken:{},
        linToken:{}
    },
    bankData: {
        bankAccount: String,
        accountNumber: Number,
        accountType: String, //Definir el uso
        bank: {
            type: mongoose.Schema.Types.ObjectId,
            ref:'Bank'
        }
    },
    nextPayment: {
        type: Date,
        required:true
    },
    location: {
        lat: String,
        long: String
    },
    postalCode: {
        type: Number
    },
    banners: {
        type: [String]
    },
    payments: {
        paypal: {},
        stripe: {},
        visanet: {},
        paq: {},
        gyt: {},
        caex: Boolean,
        tranex: Boolean,
        infile: {}
    },
    branchOffices: {
        type: Array // TODO: Structure {name, address} Hacer Metodos
    }
}); 


companySchema
    .post('save', function(savedCompany) {
        sendEmail('daniel@insayd.com', 'info@vendalo.com', 'Creación de Dominio', 
        newDomainTemplate(savedCompany.domain, savedCompany.email, savedCompany.phone))
    })


export default mongoose.model('company', companySchema);