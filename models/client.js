// TODO: sintaxis correo sendEmail('ldcon05@gmail.com', 'info@vendalo.com', 'prueba', 'prueba')
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

import sendEmail from '../services/sendgrid';
import newUserTemplate from '../templates/mail/newUser';
import createToken from '../services/auth';


const Schema = mongoose.Schema;

const clientSchema = Schema({
    email: {
        type: String,
        required:true,
        unique:true
    }, 
    name : {
        type: String,
        required:true
    },
    lastName: {
        type: String,
        required:true
    },
    user: {
        type:String,
        required:true,
        unique:true
    },
    password: {
        type:String,
        required:true
    },
    country: {
        type:mongoose.Schema.Types.ObjectId,
        ref: 'Country'
    },
    postalCode: {
        type: Number
    },
    phone: {
        type:String,
        required:true
    },
    address:{
        type:[{
            name:String,
            addressB:String,
            country:{
                type: mongoose.Schema.Types.ObjectId,
                ref:'Country'
            },
            postalCode:String
        }]
    },
    avatar: {
        type: String,
        default: "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
    },
    tributaryId: {
        type:String
    },
    buyer: {
        type: Boolean
    },
    seller: {
        type: Boolean
    },
    higher: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Company'
    },
    cart: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Product'
    },
    wishes: {
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'Product'
    },
    authorization: {
        type:String
    }
});

//Acciones antes de guardar

clientSchema
    .pre('save', function (next) { //TODO: Aqui se uso una funcion normal ya que estas mantienen el this y las arrow no
        bcrypt
            .hash(this.password, 10)
            .then((hash) => {
                this.password = hash
                this.authorization = createToken(this.user);
                next(); 
            })  
            .catch(next)
    })
    
//Acciones luego de guardar
clientSchema
    .post('save', function (savedClient){
        sendEmail(savedClient.email, 'info@vendalo.com', 'Creación de Usuario', newUserTemplate(savedClient.name))
    })

export default mongoose.model('Client',clientSchema);