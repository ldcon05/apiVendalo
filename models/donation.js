//TODO: User si es logueado y company si es anonimo y tiene que poner los otros campos

import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const donationSchema = Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    },
    company: {
        type:mongoose.Schema.Types.ObjectId,
        ref: 'Company'
    },
    coin: {
        type:String,
        required:true
    },
    money: {
        type: mongoose.Schema.Types.Decimal,
        required:true
    },
    email: {
        type:String
    },
    date: {
        type:Date,
        default: Date.now
    },
    months: {
        type:Number
    },
    card: {
        cardNumber: Number,
        dueDate: Number,
        cvn: Number
    }
});

export default mongoose.model('Donation', donationSchema);