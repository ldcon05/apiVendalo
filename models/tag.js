import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const tagSchema = Schema({
    name : {
        type: String,
        required:true,
        unique:true
    }
});

export default mongoose.model('Tag',tagSchema);