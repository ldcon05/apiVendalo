import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const orderSchema = Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client',
        required:true
    },
    companyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'company'
    },
    products: {
        type: [{
            quantity: {
                type:Number,
                required:true
            },
            total: {
                type: mongoose.Schema.Types.Double,
                required:true
            },
            color: {
                type:String
            },
            size: {
                type:String
            },
            product: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Product"
            }
        }]
    },
    date: {
        type:Date,
        default: Date.now
    },
    status: {
        type:Number,
        required:true
    },
    address: {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'addressClient'
    },
    shipping: {
        type: Number
    },
    shippingInstructions: {
        type:String, 
        required:true
    },
    deliveryReport: {
        status:Boolean,
        comment:String,
        image: String
    }, 
    total: {
        type:Number,
        required:true
    },
    GuiaCaex: {
        noGuia: String,
        sticker: String
    }
});

export default mongoose.model('Order',orderSchema);
