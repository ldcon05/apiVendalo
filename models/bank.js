import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const bankSchema = Schema({
    name : {
        type:String,
        required:true,
        unique:true
    },
    logo : {
        type: String,
        required: true
    }
});

export default mongoose.model("Bank",bankSchema);