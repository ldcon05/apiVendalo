import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const contrySchema = Schema({
    name: {
        type:String,
        required:true,
        unique:true
    },
    countryCode: {
        type:Number,
        required:true
    },
    languaje: {
        type:String,
        required:true
    },
    coin: {
        type:String,
        required:true
    },
    abbr: {
        type:String,
        required:true
    }
});

export default mongoose.model("Country",contrySchema);


