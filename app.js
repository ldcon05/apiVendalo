import express from "express";
import bodyParser from "body-parser";
import cors from 'cors';

import routerCompany from "./routes/routerCompany";
import routerCountry from "./routes/routerCountry";
import routerBank from "./routes/routerBank";
import routerClient from "./routes/routerClient";
import routerAddressesClient from './routes/routerAddress';
import routerProduct from "./routes/routerProduct";
import routerTag from './routes/routerTag';
import routerCategory from './routes/routerCategory';
import routerCart from './routes/routerCart';
import routerOrder from './routes/routerOrder';
import routerRefund from './routes/routerRefund';
import routerDonation from './routes/routerDonation';
import anonymous from './routes/routerAnonymous';


import requestError from "./middlewares/requestError";
import auth from "./middlewares/auth";

const app = express();
const publicApi = '/';
const privateApi = '/api';

app.use(bodyParser.urlencoded({ extended:false }));
app.use(bodyParser.json());
app.use(cors())

app.use(publicApi, anonymous)

// app.use(privateApi, auth);
app.use(privateApi, routerCompany);
app.use(privateApi, routerCountry);
app.use(privateApi, routerBank);
app.use(privateApi, routerProduct);
app.use(privateApi, routerClient);
app.use(privateApi, routerAddressesClient);
app.use(privateApi, routerTag);
app.use(privateApi, routerCategory);
app.use(privateApi, routerCart);
app.use(privateApi, routerOrder)
app.use(privateApi, routerRefund)
app.use(privateApi, routerDonation)

app.use(requestError);

export default app;