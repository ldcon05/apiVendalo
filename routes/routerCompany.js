import express from 'express';
import {getCompanies,addCompany, getCompany, getCompanyDomain, updateCompany, deleteCompany, updateTokens, updatePayments, uploadLogo, uploadIcon} from '../controllers/companyCtrl';
import multipart from 'connect-multiparty';

const api = express.Router();
const multipartMiddleware = multipart();

//Company
api.get('/companies',getCompanies)
api.get('/companies/:companyId',getCompany)
api.get('/companies/domain/:domainCompany',getCompanyDomain)

api.post('/companies',addCompany)

api.put('/companies/:companyId', updateCompany)
api.put('/companies/token/:companyId', updateTokens)
api.put('/companies/payments/:companyId', updatePayments)
api.put('/companies/logo/:companyId', multipartMiddleware, uploadLogo)
api.put('/companies/icon/:companyId', multipartMiddleware, uploadIcon)

api.delete('/companies/:companyId', deleteCompany)

export default api;