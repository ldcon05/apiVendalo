import express from 'express';
import {getCategories, addCategory, findCategoryByName, deleteCategory} from '../controllers/categoryCtrl';

const api = express.Router();

api.get('/categories/:companyId', getCategories);
api.get('/categories/find/:companyId/:categoryName',findCategoryByName)

api.post('/categories', addCategory);

api.delete('/categories/:categoryId' , deleteCategory);

export default api;

