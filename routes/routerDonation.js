import express from 'express';
import {getDonations, addDonation} from '../controllers/donationCtrl';

const api = express.Router();

api.get('/donations', getDonations);

export default api;

