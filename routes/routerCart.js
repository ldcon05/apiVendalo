import express from 'express';
import { getCart, addCart, getCartWithClientId, addProductCart, updateCart, removeProductCart, removeCart} from '../controllers/cartCtrl';

const api = express.Router();

api.get('/cart', getCart);
api.get('/cart/:clientId', getCartWithClientId);
api.post('/cart', addCart);
api.put('/cart/:cartId', addProductCart);
api.put('/cart/updateItem/:cartId', updateCart);
api.put('/cart/removeItem/:cartId/:productId', removeProductCart);
api.delete('/cart/:cartId', removeCart);


export default api;