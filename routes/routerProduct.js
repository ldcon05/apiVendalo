import express from 'express';
import multipart from 'connect-multiparty';

import {getProducts, getProduct, addProduct, getProductByCompany, 
        updateProduct, deleteProduct, uploadImage, getProductByCategory,
        findProduct, customSearch, deleteImage, uploapColor, deleteCustomColor, 
        uploapColorNoImage, countProducts} from '../controllers/productCtrl';

const api = express.Router();
const multipartMiddleware = multipart();

api.get('/products', getProducts);
api.get('/products/fcompany/:companyId/:pagination', getProductByCompany);
api.get('/products/category/:categoryId', getProductByCategory);
api.get('/products/find/:keyword/:companyId', findProduct);
api.get('/products/csearch/:keyword/:greater/:less/:companyId', customSearch);
api.get('/products/:productId', getProduct);
api.get('/count/products/:companyId', countProducts)

api.post('/products',addProduct);
 
api.put('/products/:productId', updateProduct);
api.put('/productimg/:productId', multipartMiddleware, uploadImage)
api.put('/customcolor/:productId', multipartMiddleware, uploapColor)
api.put('/customcolorN/:productId', uploapColorNoImage)

api.delete('/products/:productId', deleteProduct)
api.delete('/productimg/:productId', deleteImage)
api.delete('/customcolor/:productId', deleteCustomColor)

export default api;