import express from 'express';
import {getCountries, addCountry} from '../controllers/countryCtrl';

const api = express.Router();

api.get('/countries', getCountries);
api.post('/countries', addCountry);

export default api;