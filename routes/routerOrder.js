/**
 * TODO: Parametrtos para pagar con tarjeta : Datos de la tarjeta, empresa, direccion 
 */

import express from 'express';
import {getOrders, addOrder, payWithCard, createInvoice, getOrder, 
    getOrdersByClient, updateOrder, buildInvoiceProducts, getCodeCity, 
    getShippingPackage, buildShipping, getTrackingGuia} from '../controllers/orderCtrl';

const api = express.Router();

api.get('/orders', getOrders)
api.get('/order/:orderId', getOrder)
api.get('/orders/:clientId', getOrdersByClient)
api.get('/order/generate/:orderId', buildInvoiceProducts)
api.get('/shipping/:orderId', buildShipping)
api.get('/tracking/order/:guia', getTrackingGuia)

api.post('/orders', addOrder);
api.post('/orders/:orderId', payWithCard)
api.post('/cities', getCodeCity)

api.put('/order/:orderId', updateOrder)
api.put('/order/shipping/:orderId', getShippingPackage)

export default api;
