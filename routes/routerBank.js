import express from 'express';
import {getBanks, addBank} from '../controllers/bankCtrl';

const api = express.Router();

api.get('/banks',getBanks);
api.post('/banks',addBank);

export default api;