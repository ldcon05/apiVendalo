import express from 'express';
import {getRefunds, addRefund} from '../controllers/refundCtrl';

const api = express.Router();

api.get('/refunds', getRefunds);

export default api;

