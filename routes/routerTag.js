import express from 'express';
import {getTags, addTag} from '../controllers/tagCtrl';

const api = express.Router();

api.get('/tags', getTags);
api.post('/tags', addTag);


export default api;

