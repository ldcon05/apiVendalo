import express from 'express';
import {getAddressClient, addAddressClient, updateAddressClient, deleteAddressClient} from '../controllers/addressCtrl';

const api = express.Router();

api.get('/addresses/:clientId', getAddressClient);
api.post('/addresses', addAddressClient);
api.put('/addresses/:addressId', updateAddressClient);
api.delete('/addresses/:addressId', deleteAddressClient);

export default api;

