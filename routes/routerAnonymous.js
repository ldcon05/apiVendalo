import express from 'express';

import {getCompanies, getCompanyDomain} from '../controllers/companyCtrl';
import {addClient, getClientByEmail, Login} from '../controllers/clientCtrl';
import {getProducts, getProduct, getProductByCompany} from '../controllers/productCtrl';

const api = express.Router();

//Company
api.get('/companies',getCompanies);
api.get('/companies/domain/:domainCompany',getCompanyDomain);

//User
api.get('/login/email/:email', getClientByEmail);
api.post('/register', addClient);
api.post('/login', Login);

//Product
api.get('/products', getProducts);
api.get('/products/fcompany/:companyId', getProductByCompany);
api.get('/products/:productId', getProduct);




export default api;