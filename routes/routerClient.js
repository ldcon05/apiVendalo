import express from 'express';
import multipart from 'connect-multiparty';
import {getClients, getClient, addClient, getClientByEmail, Login, updateClient, uploadImage, updatePassword} from '../controllers/clientCtrl';

const api = express.Router();
const multipartMiddleware = multipart();

//Clients
api.get('/clients', getClients);
api.get('/clients/:clientId', getClient);
api.get('/clients/email/:email', getClientByEmail);

api.post('/clients', addClient);
api.post('/login', Login);

api.put('/clients/:clientId', updateClient);
api.put('/uploaduser/:clientId/', multipartMiddleware,  uploadImage)
api.put('/updatePassword/:clientId', updatePassword)

export default api; 