import aws from 'aws-sdk';
import file from 'fs';

aws.config.loadFromPath('./aws.json');
const s3 = new aws.S3();

function createBucket (name) {
    s3.createBucket({ Bucket: name }, function (err, data) {
        if(err) 
            console.log(err)
    });
}

function uploadS3 (image, imageName, bucketName) {
    let params = { 
        Bucket: bucketName, 
        Key: `${imageName}.png`, 
        ACL: 'public-read',
        Body: file.createReadStream(image)
    };
    s3.putObject(params, 
        function (err, data) {
            if (err) 
                return false
        }
    ) 
    return true
}

function removeObjectS3 (key, bucketName) {
    let params = { 
        Bucket: bucketName, 
        Key: `${key}`
    };
    s3.deleteObject(params,
        function(err, data) {
            if(err || data == {})
                return false
        }
    )
    return true
}

export {createBucket, uploadS3, removeObjectS3}