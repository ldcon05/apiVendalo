
function domainMessageTemplate (domain, email, phone) {
  let newDomainMessage = `
  <html lang="es">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="https://mi.vendalo.com/css/mail.css">
  </head>
  <body style="margin: 0;">
      
    <table class="entire-page" style="background: #E7E6F7;width: 100%;padding: 20px 0;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;line-height: 1.5;">
    <tr>
      <td style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;">
        <table class="email-body" style="max-width: 600px;min-width: 320px;margin: 0 auto;background: white;border-collapse: collapse;">
          <tr>
            <td class="email-header" style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;background: #234CA1;padding: 30px;text-align:center;">
              <a href="https://vendalo.com" style="color: #2F82DE;font-weight: bold;text-decoration: none;">
                <img src="https://vendalo.com/assets/img/vendalo_correo.png" style="max-width: 300px;" alt="Vendalo">
              </a>
            </td>
          </tr> <!-- header -->
          <tr>
          </tr> <!-- feature images -->
          <tr>
            <td class="news-section" style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;padding: 20px 30px; text-align:justify;">
              <h2 style="color: black;font-size: 24px;">Creación de Dominio</h2>
              <span class="center" style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif; "> Se ha solicitado la creación de un nuevo subdominio, a continuacion te estaremos detallando lo solicitado.
   </span>
     <br>
      
    <table style='max-width: 600px;min-width: 320px;margin: 0 auto;background: white;border-collapse: collapse;'>
    <tr>
      <h3 style="text-align:center;margin:2em 0em 1em">Datos del solicitante</h3>
    </tr>
    <tr>
      <td style='min-width:7em; margin-left:20em'>
        <p><b>Dominio:</b></p>
        <p><b>Correo:</b></p>
        <p><b>Telefono:</b></p>
      </td>
      <td>
        <p>${domain}</p>
        <p>${email}</p>
        <p>${phone}</p>
      </td>
    </tr>
    </table>
          <tr>
            <td class="news-section" style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;padding: 20px 30px;">
            </td>
          </tr>
          <tr>
            <td class="footer" style="font-size: 10px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;background: #eee;padding: 10px;text-align: center;">
              Recibiste se ha solicitado crear un subdominio.
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </body>
  </html>
  `;

  return newDomainMessage;
}


export default domainMessageTemplate;