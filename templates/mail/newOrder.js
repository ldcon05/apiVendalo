const orderEmailTemplate = `
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://mi.vendalo.com/css/mail.css">
</head>
<body style="margin: 0;">
    
  <table class="entire-page" style="background: #E7E6F7;width: 100%;padding: 20px 0;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;line-height: 1.5;">
  <tr>
    <td style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;">
      <table class="email-body" style="max-width: 600px;min-width: 320px;margin: 0 auto;background: white;border-collapse: collapse;">
        <tr>
          <td class="email-header" style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;background: #234CA1;padding: 30px;text-align:center;">
            <a href="https://vendalo.com" style="color: #2F82DE;font-weight: bold;text-decoration: none;">
              <img src="https://vendalo.com/assets/img/vendalo_correo.png" style="max-width: 300px;" alt="Vendalo">
            </a>
          </td>
        </tr> <!-- header -->
        <tr>
        </tr> <!-- feature images -->
        <tr>
          <td class="news-section" style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;padding: 20px 30px; text-align:justify;">
            <h2 style="color: black;font-size: 24px;">Detalle de Compra</h2>
            <span class="center" style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif; "> A continuación se le mostrara un listado de los productos a adquir ademas de infórmacion de ayuda.
 </span>
   <br>
    
  <table class="best-of-table" style="border-collapse: collapse;">
  <tr>
    <h3 style="text-align:center;margin:2em 0em 1em"># de Orden: 45 </h3>
  </tr>
  <tr>
    <td style='min-width:7em; margin-left:20em'>
      
      <div style='display:flex;'>
         <div style='width:40% ' >
        <h5>INFORMACIÓN DE VENDEDOR</h5>
        <img src="https://vendalo.com/emailTemplates/user02.png" style="max-width: 100%; min-width:7em" alt="Propietary">
      </div>
        <div style='margin:1.5em 0em 5em 2em'>
          <p style='font-weight:bolder; margin: 0px'>Nombre de cuenta :</p>
          <p style='margin:0px 0px 1em 0px'>Joseph Lee Daniels Celada</p>

          <p style='font-weight:bolder; margin: 0px'>Tipo de Cuenta:</p>
          <p style='margin:0px 0px 1em 0px'>Ahorros</p>

          <p style='font-weight:bolder; margin: 0px'>No. de Cueta:</p>
          <p style='margin:0px 0px 1em 0px'>5810205227</p>

          <p style='font-weight:bolder; margin: 0px'>Banco:</p>
          <p style='margin:0px 0px 1em 0px'>Banco G&T Continental</p>
        </div>
      </div>
      
      <div style='display:flex;margin-bottom:5em;'>
        <div style='width:40%'>
          <h5>METODO DE PAGO</h5>
          <img src="https://vendalo.com/emailTemplates/shipping.png" style="max-width: 100%; min-width:7em" alt="Paymaent">
        </div>
         <div style='margin:3.5em 0em 5em 2em'>
           <p style='font-weight:bolder; margin: 0px'>Medios:</p>
           <p style='margin:0px 0px 1em 0px'>Tarjeta de Débito o Crédito</p> 
        </div>
      </div>
      
      <div style='display:flex;margin-bottom:5em;'>
        <div style='width:40%'>
          <h5>METODO DE ENVíO</h5>
          <img src="https://vendalo.com/emailTemplates/map.png" style="max-width: 100%; min-width:7em" alt="Paymaent">
        </div>
         <div style='margin:3.5em 0em 5em 2em'>
           <p style='font-weight:bolder; margin: 0px'>Medios:</p>
           <p style='margin:0px 0px 1em 0px'> Cargo Expreso o Propio </p> 
        </div>
      </div>
      
      <div style='display:flex;margin-bottom:5em;'>
        <div style='width:40%'>
          <h5>INFORMACIÓN DE CLIENTE</h5>
          <img src="https://vendalo.com/emailTemplates/user.png" style="max-width: 100%; min-width:7em" alt="Paymaent">
        </div>
         <div style='margin:3.5em 0em 5em 2em'>
          <p style='font-weight:bolder; margin: 0px'>Nombre :</p>
          <p style='margin:0px 0px 1em 0px'>Daniel Con</p>

          <p style='font-weight:bolder; margin: 0px'>Correo :</p>
          <p style='margin:0px 0px 1em 0px'>ldcon05@gmail.com</p>

          <p style='font-weight:bolder; margin: 0px'>ID Tributario:</p>
          <p style='margin:0px 0px 1em 0px'>91942357</p>

          <p style='font-weight:bolder; margin: 0px'>Dirección :</p>
          <p style='margin:0px 0px 1em 0px'>SAN JOSE PINULA, 5C 8-38 , Zona 3</p>
           
          <p style='font-weight:bolder; margin: 0px'>Codigo postal :</p>
          <p style='margin:0px 0px 1em 0px'>SAN JOSE PINULA, 5C 8-38 , Zona 3</p>
           
          <p style='font-weight:bolder; margin: 0px'>Telefono :</p>
          <p style='margin:0px 0px 1em 0px'>50066003</p>
        </div>
      </div>

    </td>
  </tr>
  </table>
        <tr>
          <td class="news-section" style="font-size: 16px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;padding: 20px 30px;">
          </td>
        </tr>
        <tr>
          <td class="footer" style="font-size: 10px;color: #878787;font-family: &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, sans-serif;background: #eee;padding: 10px;text-align: center;">
            Recibiste se ha solicitado crear un subdominio.
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
`;

export default orderEmailTemplate;